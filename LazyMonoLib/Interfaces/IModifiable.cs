﻿using LazyMonoLib.Modifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Interfaces
{
  public interface IModifiable
  {
    void AddModifier(IModifier modifier);

    void RemoveModifier(IModifier modifier);

    void ClearModifier();

    bool HasModifier(IModifier modifier);

    bool HasModifierOfType<K>();

    K GetFirstModifierOfType<K>();

    List<K> GetModifierOfType<K>();

    int ModifierCount { get; }
  }
}
