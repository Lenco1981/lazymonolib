﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyMonogameLib.Project
{
	public class DummyProjectEntry : IProjectEntry
	{
		public DummyProjectEntry()
		{
			Childs = new List<IProjectEntry>();
		}

		public string DisplayName { get; set; }

		public string FullPath { get; set; }

		public Uri Icon { get; set; }

		public List<IProjectEntry> Childs { get; set; }

		public bool HasChilds
		{
			get { return Childs.Count > 0; }
		}
	}
}
