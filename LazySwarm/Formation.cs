﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazySwarm.animal;
using Microsoft.Xna.Framework;

namespace LazySwarm
{
  public class Formation
  {
    public List<Animal> AnimalList { get; private set; }
    public Rectangle Screen { get; private set; }
    public Grid Grid { get; private set; }

    public Formation(Rectangle screenRectangle, int slits)
    {
      this.AnimalList = new List<Animal>();
      this.Screen = screenRectangle;
      this.Grid = new Grid(screenRectangle, slits);
    }

    public IEnumerable<Animal> findNeighbors(Animal current)
    {
      IEnumerable<Animal> sequence;
      float distance = current.AnimalSpec.DetectionDistance;
      sequence = Grid.encompassNeighbors(current, distance);
      return sequence;
    }

    public void normalize(ref Vector2 position, out Vector2 normalized)
    {
      normalized = position;
      if (position.X < Screen.Left)
        normalized.X += Screen.Width;
      else if (Screen.Right <= position.X)
        normalized.X -= Screen.Width;

      if (position.Y < Screen.Top)
        normalized.Y += Screen.Height;
      else if (Screen.Bottom <= position.Y)
        normalized.Y -= Screen.Height;
    }

    public void move(Animal animal, Vector2 newPosition)
    {
      Grid.remove(animal);
      animal.Position = newPosition;
      Grid.add(animal);
    }

    public void add(Animal newone)
    {
      AnimalList.Add(newone);
      Grid.add(newone);
    }

    public void remove(Animal oldone)
    {
      AnimalList.Remove(oldone);
      Grid.remove(oldone);
    }
  }
}
