﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;

namespace LazyMonoLib
{
  /// <summary>
  /// Abstract base class for all drawable and updateable entities
  /// </summary>
  public abstract class Entity : Interfaces.ILazyDrawable, Interfaces.ILazyUpdateable, Interfaces.IEntity
  {
    protected string name = String.Empty;
    protected Rectangle? sourceRectangle = null;
    protected Vector2 position = Vector2.Zero;
    protected Vector2 size = Vector2.Zero;
    protected Color coloration = Color.White;
    protected bool deleteMe = false;
    protected bool isVisible = true;
    protected bool doUpdate = true;

    public Entity()
    { }

    public Entity(string name)
    {
      Name = name;
    }

    /// <summary>
    /// Gets or sets an optional name to the entity.
    /// </summary>
    public string Name
    {
      get { return name; }
      set
      {
        if (name != value)
        {
          name = value;
          nameChanged();
        }
      }
    }

    /// <summary>
    /// Gets or sets the current entity postion relative to the top left corner of the entity
    /// </summary>
    public Vector2 Position
    {
      get { return position; }
      set
      {
        if (position != value)
        {
          position = value;
          positionChanged();
        }
      }
    }

    /// <summary>
    /// Gets or sets the current entity position relative to the entity's center
    /// </summary>
    public Vector2 CenterPosition
    {
      get
      {
        return new Vector2(
          Position.X + (Size.X / 2f),
          Position.Y + (Size.Y / 2f));
      }
      set
      {
        Position = new Vector2(
          value.X - (Size.X / 2f),
          value.Y - (Size.Y / 2f));
      }
    }

    /// <summary>
    /// Gets or sets the size the current entity will be drawn with
    /// </summary>
    public Vector2 Size
    {
      get { return size; }
      set
      {
        if (size != value)
        {
          size = value;
          sizeChanged();
        }
      }
    }

    /// <summary>
    /// Gets or sets a color to colorize the current texture. Set Coloration to Color.White to render
    /// the texture as is.
    /// </summary>
    public Color Coloration
    {
      get { return coloration; }
      set
      {
        if (coloration != value)
        {
          coloration = value;
          colorationChanged();
        }
      }
    }

    /// <summary>
    /// Gets or sets the source rectangle defining which part of an assigned texture will be rendered.
    /// Can be null to render the entire texture.
    /// </summary>
    public Rectangle? SourceRectangle
    {
      get { return sourceRectangle; }
      set
      {
        if (sourceRectangle != value)
        {
          sourceRectangle = value;
          sourceRectangleChanged();
        }
      }
    }

    /// <summary>
    /// Indicates if the current entity is visible
    /// </summary>
    public bool IsVisible
    {
      get { return isVisible; }
      set {
        if (isVisible != value)
        {
          isVisible = value;
          visibleChanged();
        }
      }
    }

    /// <summary>
    /// If set to true the entity will be removed from the draw- and update routine on the next iteration
    /// </summary>
    public bool DeleteMe
    {
      get { return deleteMe; }
      set
      {
        if (deleteMe != value)
        {
          deleteMe = value;
          deleteMeChanged();
        }
      }
    }

    /// <summary>
    /// If set to false the entity will be skipped during the update routine
    /// </summary>
    public bool DoUpdate
    {
      get { return doUpdate; }
      set {
        if (doUpdate != value)
        {
          doUpdate = value;
          doUpdateChanged();          
        }
      }
    }

    /// <summary>
    /// Gets the destination rectangle the entity will be drawn to
    /// </summary>
    protected Rectangle destinationRectangle
    {
      get { return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y); }
    }

    /// <summary>
    /// Virtual method executed if the position changes. The base method is empty so there's no need to call the base method.
    /// </summary>
    protected virtual void positionChanged()
    { }

    /// <summary>
    /// Virtual method exectued if the name changees
    /// </summary>
    protected virtual void nameChanged()
    { }

    protected virtual void colorationChanged()
    { }

    protected virtual void sizeChanged()
    { }

    protected virtual void sourceRectangleChanged()
    { }

    protected virtual void deleteMeChanged()
    { }

    protected virtual void visibleChanged()
    { }

    protected virtual void doUpdateChanged()
    { }

    /// <summary>
    /// Returns the destination rectangle for drawing this entity. Transformed by the given camera
    /// </summary>
    protected Rectangle getDestinationRectangle(ICamera camera)
    {
      Rectangle r = new Rectangle((int)position.X, (int)position.Y, (int)Size.X, (int)Size.Y);

      return camera.Transform(r);
    }

    public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera);

    public abstract void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera);

    public bool IsHitBy(Point point, ICamera camera)
    {
      return IsHitBy(point.X, point.Y, camera);
    }

    public bool IsHitBy(Vector2 vector, ICamera camera)
    {
      return IsHitBy((int)vector.X, (int)vector.Y, camera);
    }

    public bool IsHitBy(int x, int y, ICamera camera)
    {
      return
        x >= camera.Transform(position).X &&
        x <= camera.Transform(position).X + size.X &&
        y >= camera.Transform(position).Y &&
        y <= camera.Transform(position).Y + size.Y;
    }

    public bool IsHitBy(MouseState mouseState, ICamera camera)
    {
      return IsHitBy(mouseState.X, mouseState.Y, camera);
    }

    public override string ToString()
    {
      string type = this.GetType().Name;

      if (name.IsFilled())
        return String.Format("[{0}]: {1}", type, name);
      else
        return String.Format("[{0}]: no-name", type);
    }
  }
}
