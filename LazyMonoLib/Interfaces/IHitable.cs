﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib.Interfaces
{
  public interface IHitable
  {
    bool IsHitBy(Point point);

    bool IsHitBy(Vector2 vector);

    bool IsHitBy(int x, int y);

    bool IsHitBy(MouseState mouseState);
  }
}
