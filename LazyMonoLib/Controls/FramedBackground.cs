﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework.Input;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Controls
{
  public class FrameBackground : Control
  {
    protected string topLeftSubtextureName = "topleft";
    protected string topSubtextureName = "topcenter";
    protected string topRightSubtextureName = "topright";

    protected string leftSubtextureName = "left";
    protected string centerSubtextureName = "center";
    protected string rightSubtextureName = "right";
    
    protected string bottomLeftSubtextureName = "bottomleft";
    protected string bottomSubtextureName = "bottomcenter";
    protected string bottomRightSubtextureName = "bottomright";

    private SubtextureDefinition topLeftSub { get { return FrameSprite.Subtextures[topLeftSubtextureName]; } }
    private SubtextureDefinition topSub { get { return FrameSprite.Subtextures[topSubtextureName]; } }
    private SubtextureDefinition topRightSub { get { return FrameSprite.Subtextures[topRightSubtextureName]; } }

    private SubtextureDefinition leftSub { get { return FrameSprite.Subtextures[leftSubtextureName]; } }
    private SubtextureDefinition centerSub { get { return FrameSprite.Subtextures[centerSubtextureName]; } }
    private SubtextureDefinition rightSub { get { return FrameSprite.Subtextures[rightSubtextureName]; } }

    private SubtextureDefinition bottomLeftSub { get { return FrameSprite.Subtextures[bottomLeftSubtextureName]; } }
    private SubtextureDefinition bottomSub { get { return FrameSprite.Subtextures[bottomSubtextureName]; } }
    private SubtextureDefinition bottomRightSub { get { return FrameSprite.Subtextures[bottomRightSubtextureName]; } }

    private Rectangle topLeftDestinationRectangle;
    private Rectangle topDestinationRectangle;
    private Rectangle topRightDestinationRectangle;

    private Rectangle leftDestinationRectangle;
    private Rectangle centerDestinationRectangle;
    private Rectangle rightDestinationRectangle;

    private Rectangle bottomLeftDestinationRectangle;
    private Rectangle bottomDestinationRectangle;
    private Rectangle bottomRightDestinationRectangle;

    public Sprite FrameSprite { get; set; }

    public FrameBackground(string controlName, string spriteBundleName)
      : base(controlName)
    {
      FrameSprite = new Sprite();
      FrameSprite.SetTextureBundle(Global.ContentProvider.GetTextureBundle(spriteBundleName));
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(topLeftDestinationRectangle), topLeftSub.Rectangle, coloration);
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(topDestinationRectangle), topSub.Rectangle, coloration);
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(topRightDestinationRectangle), topRightSub.Rectangle, coloration);

      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(leftDestinationRectangle), leftSub.Rectangle, coloration);
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(centerDestinationRectangle), centerSub.Rectangle, coloration);
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(rightDestinationRectangle), rightSub.Rectangle, coloration);

      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(bottomLeftDestinationRectangle), bottomLeftSub.Rectangle, coloration);
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(bottomDestinationRectangle), bottomSub.Rectangle, coloration);
      spriteBatch.Draw(FrameSprite.Texture, camera.Transform(bottomRightDestinationRectangle), bottomRightSub.Rectangle, coloration);
    }

    protected override void sizeChanged()
    {
      updateRectangles();
    }

    protected override void positionChanged()
    {
      updateRectangles();
    }

    private void updateRectangles()
    {
      topLeftDestinationRectangle = new Rectangle((int)position.X, (int)position.Y, topLeftSub.Width, topLeftSub.Height);
      topDestinationRectangle = new Rectangle((int)position.X + topLeftSub.Width, (int)position.Y, (int)Size.X - (topLeftSub.Width + topRightSub.Width), topSub.Height);
      topRightDestinationRectangle = new Rectangle((int)position.X + ((int)Size.X - topRightSub.Width), (int)position.Y, topRightSub.Width, topSub.Height);

      leftDestinationRectangle = new Rectangle((int)position.X, (int)(position.Y + topRightSub.Height), leftSub.Width, (int)Size.Y - (topLeftSub.Height + bottomLeftSub.Height));
      centerDestinationRectangle = new Rectangle((int)position.X + leftSub.Width, (int)position.Y + topSub.Height, (int)Size.X - (topLeftSub.Width + topRightSub.Width), (int)Size.Y - (topLeftSub.Height + bottomLeftSub.Height));
      rightDestinationRectangle = new Rectangle((int)position.X + ((int)Size.X - rightSub.Width), (int)(position.Y + topRightSub.Height), rightSub.Width, (int)Size.Y - (topLeftSub.Height + bottomLeftSub.Height));

      bottomLeftDestinationRectangle = new Rectangle((int)position.X, (int)(position.Y + (Size.Y-topLeftSub.Height)), bottomLeftSub.Width, bottomLeftSub.Height);
      bottomDestinationRectangle = new Rectangle((int)position.X + bottomLeftSub.Width, (int)(position.Y + (Size.Y - topSub.Height)), (int)Size.X - (bottomLeftSub.Width + bottomRightSub.Width), bottomSub.Height);
      bottomRightDestinationRectangle = new Rectangle((int)position.X + ((int)Size.X - bottomRightSub.Width), (int)(position.Y + (Size.Y - topLeftSub.Height)), bottomRightSub.Width, bottomRightSub.Height);
    }
  }
}
