﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// This modifier pauses the modifier execution for a defined amount of time
  /// </summary>
  public class DoNothingModifier : IModifier
  {
    private TimeSpan duration;

    public DoNothingModifier(int seconds)
      : this(new TimeSpan(0, 0, seconds))
    { }

    public DoNothingModifier(TimeSpan duration)
    {
      this.duration = duration;
    }

    public void NextStep(ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      duration = duration - gameTime.ElapsedGameTime;
    }

    public bool IsFinished
    {
      get { return duration.Ticks <= 0; }
    }
  }
}
