﻿using LazyMonoLib;
using LazyMonoLib.Controls;
using LazyMonoLib.Enums;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Views
{
  public class MessageBox1stTry : AbstractView, ILazyDialogView
  {
    public MessageBox1stTry()
    {
      DialogResult = LazyMonoLib.Enums.DialogResult.None;
      DialogSize = new Microsoft.Xna.Framework.Vector2(400, 200);
      CenterLocation = Global.ScreenCenter;

      InitializeBackground();
    }

    private FramedControl frame;

    public override void InitializeBackground()
    {
      frame = new FramedControl("backgroundFrame", "message_box");      
      frame.Size = new Microsoft.Xna.Framework.Vector2(400, 200);
      
      Background.Add(frame);
    }

    public DialogResult DialogResult { get; protected set; }

    public Vector2 DialogSize { get; protected set; }

    public Vector2 CenterLocation { get; protected set; }

    public Vector2 Position { get { return new Vector2(CenterLocation.X - (DialogSize.X /2f), CenterLocation.Y - (DialogSize.Y/2f)); } }

    public override void InitializeUI()
    {
      
    }

    public override void InitializeScene()
    {
      
    }

    private KeyboardState oldState = new KeyboardState();

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      base.Update(gameTime, keyboardState, mouseState, gamePadState);

      if (oldState.IsKeyDown(Keys.O) && keyboardState.IsKeyUp(Keys.O))
        okPressed();

      oldState = keyboardState;
    }

    private void okPressed()
    {
      this.DialogResult = LazyMonoLib.Enums.DialogResult.OK;
    }
  }
}
