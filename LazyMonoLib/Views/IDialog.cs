﻿using LazyMonoLib.Enums;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.EventHandler;

namespace LazyMonoLib.Views
{
  public interface ILazyDialogView : IView
  {
    DialogResult DialogResult { get; set; }

    Vector2 Size { get; set; }

    Vector2 CenterPosition { get; set; }

    Vector2 Position { get; set; }

    event DialogEventHandler Opening;

    event DialogEventHandler Closing;
 
  }
}
