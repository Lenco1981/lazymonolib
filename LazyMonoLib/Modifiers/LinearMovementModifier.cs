﻿using LazyMonoLib.Enums;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Performs a linear movement between departure and destination points using the given speed
  /// </summary>
  public class LinearMovementModifier : SpriteAnimation
  {
    private Vector2 delta = Vector2.Zero;
    
    private int xStep = 0;
    private int yStep = 0;

    private bool xFinished = false;
    private bool yFinished = false;

    private int xIterations = 0;
    private int yIterations = 0;

    public ISprite Sprite  { get; private set; }

    public Vector2 Destination { get; private set; }
    public Vector2 Departure { get; private  set; }

    //public float MovementDirectionInRadians { get; private set; }

    public Vector2 Direction { get; private set; }

    public float Speed { get; private set; }




    /// <summary>
    /// Starts a linear movement from the given departure to the given destination
    /// </summary>
    public LinearMovementModifier(ISprite sprite, Vector2 departure, Vector2 destination, float speed)
    {
      Sprite = sprite;     
      Destination = destination;      
      Departure = departure;      
      Speed = speed;      
      delta = Vector2.Zero;
      initializeWalking();
    }

    /// <summary>
    /// Starts a linear movement using the current sprite position as departure
    /// </summary>
    public LinearMovementModifier(ISprite sprite, Vector2 destination, float speed)
      : this(sprite, sprite.CenterPosition, destination, speed)
    { }

    private void initializeWalking()
    {
      xIterations = calcIterations(Departure.X, Destination.X);
      yIterations = calcIterations(Departure.Y, Destination.Y);

      delta.X = calcDelta(Departure.X, Destination.X);
      delta.Y = calcDelta(Departure.Y, Destination.Y);

       calcMovementDirection();
    }

    private void calcMovementDirection()
    {
      Vector2 differenz = Destination - Departure;

      //MovementDirectionInRadians = (float)Math.Atan2(differenz.Y, differenz.X);

      differenz.Normalize();

      differenz.Y = differenz.Y * -1;

      Direction = differenz;
    }

    private int calcIterations(float start, float end)
    {
      return (int)((MathHelper.Max(start, end) - MathHelper.Min(start, end)) / Speed);
    }

    private float calcDelta(float start, float end)
    {
      if (end > start)
        return Speed;
      else if (start > end)
        return -Speed;
      else
        return 0f;
    }

    public CompassFourth DirectionAsCompassFourth()
    {
      return MoreMathHelper.DirectionAsCompassFourth(Direction);
    }


    public override void NextStep(ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      float xAdd = 0;
      float yAdd = 0;

      if (xStep < xIterations)
      {
        xStep++;
        xAdd = delta.X;
      }
      else
        xFinished = true;

      if (yStep < yIterations)
      {
        yStep++;
        yAdd = delta.Y;
      }
      else
        yFinished = true;

     Sprite.CenterPosition = new Vector2(Sprite.CenterPosition.X + xAdd, Sprite.CenterPosition.Y + yAdd);    

    }

    public override bool IsFinished
    {
      get
      {
        return (xStep == xIterations && yStep == yIterations);
      }
    }
  }
}
