﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib;
using LazyMonoLib.Modifiers;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Sandbox.Controller;

namespace Sandbox.Views
{
  public class SplashScreenView : BaseView<SplashScreenController>
  {
    public SplashScreenView(SplashScreenController controller)
      : base(controller)
    { }

    Sprite background;

    Sprite schriftzug;

    Sprite snoreZ1, snoreZ2, snoreZ3, snoreZ4, snoreZ5, snoreZ6;   

    Sprite head;

    FadeInAnimation headFadeIn;

    ColorChangeAnimation backgroundColorChange;

    DoNothingModifier nothing;

    LinearCascadeMultiModifier headModifier;
    LinearCascadeMultiModifier schriftzugModifier;

    DoNothingModifier nothingAtEnd;

    public void StartAnimation()
    {
      background.AddModifier(backgroundColorChange);
      head.AddModifier(headModifier);
      schriftzug.AddModifier(schriftzugModifier);
    }

    public override void InitializeUI()
    {
      
    }

    private Vector2 headCenter = new Vector2((Global.ScreenWidth / 2f) - 30f, Global.ScreenHeight / 2f);

    public override void InitializeScene()
    {
      initializeSnore(snoreZ1, new Vector2(headCenter.X + 60, headCenter.Y - 140), 3, 2, new Vector2(headCenter.X + 60, headCenter.Y - 150), 0.1f);
      initializeSnore(snoreZ2, new Vector2(headCenter.X + 100, headCenter.Y - 160), 3, 2, new Vector2(headCenter.X + 100, headCenter.Y - 170), 0.1f);
      initializeSnore(snoreZ3, new Vector2(headCenter.X + 140, headCenter.Y - 180), 3, 2, new Vector2(headCenter.X + 140, headCenter.Y - 190), 0.1f);

      initializeSnore(snoreZ4, new Vector2(headCenter.X + 190, headCenter.Y - 160), 4, 2, new Vector2(headCenter.X + 190, headCenter.Y - 170), 0.1f);
      initializeSnore(snoreZ5, new Vector2(headCenter.X + 230, headCenter.Y - 180), 4, 2, new Vector2(headCenter.X + 230, headCenter.Y - 190), 0.1f);
      initializeSnore(snoreZ6, new Vector2(headCenter.X + 270, headCenter.Y - 200), 4, 2, new Vector2(headCenter.X + 270, headCenter.Y - 210), 0.1f);

      schriftzug = new Sprite();
      schriftzug.Texture = Global.ContentProvider.GetTexture2D("lazygeek_schriftzug_368x83");
      schriftzug.Size = new Vector2(368, 83);
      schriftzug.CenterLocation = new Vector2(headCenter.X + 10f, headCenter.Y + 180f);
      schriftzug.Coloration = new Color(255, 255, 255, 0);

      head = new Sprite();
      head.Texture = Global.ContentProvider.GetTexture2D("lazygeek_400x350");
      head.Size = new Vector2(175, 200);
      head.CenterLocation = headCenter;
      head.Coloration = new Color(255, 255, 255, 0);

      Scene.Add(head);
      Scene.Add(schriftzug);

      headModifier = new LinearCascadeMultiModifier();

      nothing = new DoNothingModifier(new TimeSpan(0, 0, 3));
      headFadeIn = new FadeInAnimation(new TimeSpan(0, 0, 1));
      nothingAtEnd = new DoNothingModifier(new TimeSpan(0, 0, 2));

      headModifier.ModifierList.Add(nothing);
      headModifier.ModifierList.Add(headFadeIn);
      headModifier.ModifierList.Add(nothingAtEnd);

      schriftzugModifier = new LinearCascadeMultiModifier();
      schriftzugModifier.ModifierList.Add(new DoNothingModifier(3));
      schriftzugModifier.ModifierList.Add(new FadeInAnimation(1));
      

    }

    private void initializeSnore(Sprite snore, Vector2 startPosition, int doNothingSeconds, int fadeInSpeed, Vector2 targetPosition, float movementSpeed)
    {
      snore = new Sprite();
      snore.Texture = Global.ContentProvider.GetTexture2D("lazygeek_z_104x110");
      snore.Size = new Vector2(52, 55);
      //snore.Color = Color.White;
      snore.Coloration = new Color(255, 255, 255, 0);

      snore.Position = startPosition;

      LinearCascadeMultiModifier multiModifier = new LinearCascadeMultiModifier();

      multiModifier.ModifierList.Add(new DoNothingModifier(doNothingSeconds));
      multiModifier.ModifierList.Add(new ParallelModifierList(
        new FadeInAnimation(fadeInSpeed),
        new LinearMovementModifier(snore, snore.Position, targetPosition, movementSpeed)
        ));

      snore.AddModifier(multiModifier);

      Scene.Add(snore);
    }

    public override void InitializeBackground()
    {
      backgroundColorChange = new ColorChangeAnimation(Color.Black, Color.White, new TimeSpan(0, 0, 3));

      background = new Sprite();
      background.Texture = new Microsoft.Xna.Framework.Graphics.Texture2D(Global.GraphicsDevice, 1, 1);
      background.Texture.SetData(new[] { Color.White });
      background.Size = new Vector2(Global.ScreenWidth, Global.ScreenHeight);
      background.Position = new Vector2(0, 0);

      Background.Add(background);
    }


    public override void Update(GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState)
    {
      base.Update(gameTime, keyboardState, mouseState, gamePadState);

      if (nothingAtEnd.IsFinished)
      {
        Global.Game.SetCurrentController(new HauptmenüController());
      }        
    }
  }
}
