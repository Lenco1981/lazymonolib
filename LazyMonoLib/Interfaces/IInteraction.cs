﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.EventHandler;

namespace LazyMonoLib.Interfaces
{
  public interface IInteraction
  {
    /// <summary>
    /// Fires if the mouse pointer enters the sprite
    /// </summary>
    event MouseEventHandler MouseEnter;

    /// <summary>
    /// Occurs if the mouse pointer leaves the sprite area
    /// </summary>
    event MouseEventHandler MouseLeave;

    /// <summary>
    /// Occurs if the sprite is clicked with the mouse
    /// </summary>
    event MouseEventHandler MouseClick;
  }
}
