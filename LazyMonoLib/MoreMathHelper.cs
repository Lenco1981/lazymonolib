﻿using LazyMonoLib.Enums;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib
{
  /// <summary>
  /// Additional useful functions helping with mathematic problems.
  /// </summary>
  public static class MoreMathHelper
  {
    public static bool IsInOpenInterval(double value, double leftBound, double rightBound)
    {      
      return (value > leftBound && value < rightBound);
    }

    /// <summary>
    /// Checks if a value is in the closed intervall between left and right bound.
    /// This is 'true' if the value is greater or equal to the leftBound Parameter and
    /// less or equal to the rightBound Parameter.
    /// </summary>
    public static bool IsInClosedInterval(double value, double leftBound, double rightBound)
    {
      return (value >= leftBound && value <= rightBound);
    }

    /// <summary>
    /// Checks if a value is in the closed intervall between left and right bound.
    /// This is 'true' if the value is greater or equal to the leftBound Parameter and
    /// less or equal to the rightBound Parameter.
    /// </summary>
    public static bool IsInClosedInterval(decimal value, decimal leftBound, decimal rightBound)
    {
      return (value >= leftBound && value <= rightBound);
    }

    /// <summary>
    /// Checks if a value is in the closed intervall between left and right bound.
    /// This is 'true' if the value is greater or equal to the leftBound Parameter and
    /// less or equal to the rightBound Parameter.
    /// </summary>
    public static bool IsInClosedInterval(float value, float leftBound, float rightBound)
    {
      return (value >= leftBound && value <= rightBound);
    }

    /// <summary>
    /// Checks if a value is in the closed intervall between left and right bound.
    /// This is 'true' if the value is greater or equal to the leftBound Parameter and
    /// less or equal to the rightBound Parameter.
    /// </summary>
    public static bool IsInClosedInterval(int value, int leftBound, int rightBound)
    {
      return (value >= leftBound && value <= rightBound);
    }

    public static bool IsInLeftClosedInterval(double value, double leftBound, double rightBound)
    {
      return (value >= leftBound && value < rightBound);
    }

    public static bool IsInRightClosedInterval(double value, double leftBound, double rightBound)
    {
      return (value > leftBound && value <= rightBound);
    }

    /// <summary>
    /// Converts degrees to radians
    /// </summary>
    public static double Rad(double degress)
    {
      return degress * (Math.PI / 180d);
    }

    /// <summary>
    /// Converts radians to degrees
    /// </summary>
    public static double Deg(double radians)
    {
      return radians * (180d / Math.PI);
    }

    /// <summary>
    /// Converts a vector into a CompassFourth enum.
    /// </summary>
    public static CompassFourth DirectionAsCompassFourth(Vector2 direction)
    {
      double angle = MoreMathHelper.VectorToDegrees(direction);

      if (angle < 45d || angle >= 315)
        return CompassFourth.East;
      else if (angle < 135 && angle >= 45)
        return CompassFourth.North;
      else if (angle < 215 && angle >= 135)
        return CompassFourth.West;
      else
        return CompassFourth.South;
    }

    /// <summary>
    /// Converts a vector to an angle in radians
    /// </summary>
    public static double VectorToDegrees(Vector2 vector)
    {
      double phi = Math.Atan2(vector.Y, vector.X);

      if (phi < 0)
        phi += (2d * Math.PI);

      return phi * (180d / Math.PI);
    }

    /// <summary>
    /// Clamps an integer. Returning the min value if the given value is less than the min value 
    /// or the max value if the given value is more than max value.
    /// </summary>
    public static int Clamp(int value, int min, int max)
    {
      if (value > max)
        return max;
      else if (value < min)
        return min;
      else return value;
    }


  }
}
