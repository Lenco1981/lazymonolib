﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Enums
{
  public enum CompassSixteenth  {North, NorthNorthEast, NorthEast, EastNorthEast, East, EastSouthEast, SouthEast, SouthSouthEast, South,SouthSouthWest, SouthWest, WestSouthWest, West, WestNorthWest, NorthWest, NorthNorthWest  }

  public enum CompassEighth { North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest }

  public enum CompassFourth { North, East, West, South }
}
