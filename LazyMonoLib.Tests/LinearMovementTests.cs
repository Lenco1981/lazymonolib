﻿using LazyMonoLib.Modifiers;
using Microsoft.Xna.Framework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Tests
{
  public class LinearMovementTests
  {
    [Test]
    public void DirectionInRadTest()
    {

      //LinearMovementModifier move = new LinearMovementModifier(null, new Vector2(0, 0), new Vector2(100, 0), 2);
      //Assert.That(move.MovementDirectionInRadians, Is.EqualTo(0));

      //move = new LinearMovementModifier(null, new Vector2(100, 0), new Vector2(0, 0), 2);
      //Assert.That(move.MovementDirectionInRadians, Is.EqualTo((float)Math.PI));

      //move = new LinearMovementModifier(null, new Vector2(0, 0), new Vector2(0, 100), 2);
      //Assert.That(move.MovementDirectionInRadians, Is.EqualTo(((float)Math.PI) / 2f));

      //move = new LinearMovementModifier(null, new Vector2(0, 100), new Vector2(0, 0), 2);
      //Assert.That(move.MovementDirectionInRadians, Is.EqualTo( 3f*((float)Math.PI) / 2f));


    }

    [Test]
    public void AtanTest()
    {
      

      for (float x = -1; x <= 1; x += 0.25f)
        for (float y = -1; y <= 1; y += 0.25f)
        {
          double phi = Math.Atan2(y, x);

          if (phi < 0)
            phi += (2d * Math.PI);

          Console.WriteLine(String.Format("x:{0,-5}, y:{1, -5} => {2:00.0000}, {3:000.000000}°", x, y, phi, (phi * 180d / Math.PI)));
        }
      //Console.WriteLine(String.Format("x:{0}, y:{1} => {2}", 0d, 1d, Math.Atan2(1, 0)));
    }

    [Test]
    public void RadToDegree()
    {
      for (double phi = 0; phi < (2d * Math.PI); phi += 0.2d)
      { 
        
      }
    }

    [Test]
    public void CompassFourthTest()
    { 
      
    }
  }
}
