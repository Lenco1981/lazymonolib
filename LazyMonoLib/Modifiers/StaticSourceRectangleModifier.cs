﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  public class StaticSourceRectangleModifier : IModifier
  {
    private bool rectangleSet = false;

    public Rectangle Rectangle { get; private set; }

    public StaticSourceRectangleModifier(Rectangle rectangleToApply)
    {
      Rectangle = rectangleToApply;
    }

    public void NextStep(Interfaces.ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      if (!rectangleSet)
      {
        sprite.SourceRectangle = Rectangle;
        rectangleSet = true;
      }
    }

    public void ReApplyRectangle()
    {
      rectangleSet = false;
    }

    public bool IsFinished
    {
      get { return false; }
    }
  }
}
