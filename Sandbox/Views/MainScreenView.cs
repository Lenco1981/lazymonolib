﻿using LazyMonoLib;
using LazyMonoLib.ContentProvider;
using LazyMonoLib.Controls;
using LazyMonoLib.Modifiers;
using LazyMonoLib.Primitives;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Sandbox.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.Entities;
using System.Timers;

namespace Sandbox.Views
{
  public class MainScreenView : BaseView<MainController>, IView
  {
    private Label labelRemainingRoundTime;
    private Label labelPause;

    private TimeSpan remainingRoundTime;
    private TimeSpan keyDownUpDelay = new TimeSpan(0, 0, 0, 0, 500);

    Person person = null;

    Sprite selection = null;

    Feld[,] spielfeld = null;

    

    private RectangleConstantTimeAnimation walkingSouthAnimation = new RectangleConstantTimeAnimation();

    private RectangleConstantTimeAnimation walkingEastAnimation = new RectangleConstantTimeAnimation();

    private RectangleConstantTimeAnimation walkingNorthAnimation = new RectangleConstantTimeAnimation();

    private RectangleConstantTimeAnimation walkingWestAnimation = new RectangleConstantTimeAnimation();

    public MainScreenView(MainController controller)
      : base(controller)
    {

    }

    public override void InitializeUI()
    {
      labelRemainingRoundTime = new Label();
      labelRemainingRoundTime.Font = Global.ContentProvider.GetFont("calibri", 14f);
      labelRemainingRoundTime.TextColor = Color.White;
      labelRemainingRoundTime.CenterLocation = new Vector3(Global.ScreenWidth / 2f, 20, 0);

      UI.Add(labelRemainingRoundTime);

      labelPause = new Label();
      labelPause.Font = Global.ContentProvider.GetFont("calibri", 14f);
      labelPause.TextColor = Color.White;
      labelPause.CenterLocation = new Vector3(Global.ScreenWidth / 2f, Global.ScreenHeight - 30f, 0f);
      labelPause.Visible = false;
      labelPause.Text = "PAUSE";

      UI.Add(labelPause);
    }

    private FilledRectangle rectangle;

    private HitPointBar pointBarTest;

    private HollowRectangle selectionRectangle;

    public void StartRound()
    {
      remainingRoundTime = new TimeSpan(0, 5, 0);
    }
    
    public override void InitializeScene()
    {
      pointBarTest = new HitPointBar();
      pointBarTest.Size = new Vector2(27, 5);
      pointBarTest.InnerMargin = 0f;
      pointBarTest.Position = new Vector2(100, 100);
      pointBarTest.BorderThickness = 1;
      pointBarTest.BarColor = Color.Green;
      pointBarTest.BorderColor = Color.DarkOliveGreen;
      pointBarTest.MaxPoints = 100f;
      pointBarTest.CurrentPoints = 50f;

      Scene.Add(pointBarTest);

      rectangle = new FilledRectangle();
      rectangle.FillColor = Color.Red;
      rectangle.Size = new Vector2(100, 50);
      rectangle.CenterLocation = new Vector2(300, 300);

      selectionRectangle = new HollowRectangle();
      selectionRectangle.BorderColor = Color.Lime;      
      selectionRectangle.Size = new Vector2(250, 250);      
      selectionRectangle.BorderThickness = 2;
      selectionRectangle.CenterLocation = new Vector2(100, 100);
      selectionRectangle.SetSourceRectangle(new Rectangle(50, 50, 100, 100));

      Scene.Add(rectangle);
      //Scene.Add(selectionRectangle);

      person = new Person(); //contentProvider.GetTexture("human_player_yellow").Texture2D);

      person.SetTextureBundle(Global.ContentProvider.GetTextureBundle("human_player_yellow"));

      
      
      person.Size = new Vector2(35, 35);
      person.Coloration = Color.White;
      person.SetSourceRectangle(new Rectangle(0, 0, 35, 35));

      person.CenterLocation = spielfeld[5, 5].Mitte.CenterLocation;

      Scene.Add(person);

      walkingSouthAnimation.Rectangles.Add(new Rectangle(0, 210, 35, 35));
      walkingSouthAnimation.Rectangles.Add(new Rectangle(35, 210, 35, 35));
      walkingSouthAnimation.Rectangles.Add(new Rectangle(70, 210, 35, 35));
      walkingSouthAnimation.Rectangles.Add(new Rectangle(105, 210, 35, 35));
      walkingSouthAnimation.CycleAnimation = true;
      walkingSouthAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      walkingEastAnimation.Rectangles.Add(new Rectangle(140, 210, 35, 35));
      walkingEastAnimation.Rectangles.Add(new Rectangle(175, 210, 35, 35));
      walkingEastAnimation.Rectangles.Add(new Rectangle(210, 210, 35, 35));
      walkingEastAnimation.Rectangles.Add(new Rectangle(245, 210, 35, 35));
      walkingEastAnimation.CycleAnimation = true;
      walkingEastAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      walkingNorthAnimation.Rectangles.Add(new Rectangle(280, 210, 35, 35));
      walkingNorthAnimation.Rectangles.Add(new Rectangle(315, 210, 35, 35));
      walkingNorthAnimation.Rectangles.Add(new Rectangle(350, 210, 35, 35));
      walkingNorthAnimation.Rectangles.Add(new Rectangle(385, 210, 35, 35));
      walkingNorthAnimation.CycleAnimation = true;
      walkingNorthAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      walkingWestAnimation.Rectangles.Add(new Rectangle(420, 210, 35, 35));
      walkingWestAnimation.Rectangles.Add(new Rectangle(455, 210, 35, 35));
      walkingWestAnimation.Rectangles.Add(new Rectangle(490, 210, 35, 35));
      walkingWestAnimation.Rectangles.Add(new Rectangle(525, 210, 35, 35));
      walkingWestAnimation.CycleAnimation = true;
      walkingWestAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      person.Initialize(new Rectangle(0, 0, 35, 35), walkingNorthAnimation, walkingSouthAnimation, walkingWestAnimation, walkingEastAnimation);
    }

    public override void InitializeBackground()
    {
      int rowCount = 11;
      int colCount = 7;

      spielfeld = new Feld[rowCount, colCount];

      for (int i = 0; i < rowCount; i++)
        for (int j = 0; j < colCount; j++)
        {
          Feld feld = new Feld();
          feld.Mitte.Position = new Vector2((i * 48) + 10, (j * 48) + 10);
          spielfeld[i, j] = feld;
          Background.Add(feld.Mitte);
        }
    }

    public TimeSpan? LastAttack = null;
    public TimeSpan AttackIntervall = new TimeSpan(0, 3, 0);

    private bool f5KeyHandled = false;
    private bool f4KeyHandled = false;
    private bool spaceKeyHandled = false;
    private bool leftMousePressed = false;
    private Point selectionStart;

    private bool pause = false;

    private TimeSpan spaceKeyTime;

    private KeyboardState oldKeyboardState = new KeyboardState();

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      if (keyboardState.IsKeyDown(Keys.Space) && !oldKeyboardState.IsKeyDown(Keys.Space))
      {
        // the player just pressed down
      }
      else if (keyboardState.IsKeyDown(Keys.Space) && oldKeyboardState.IsKeyDown(Keys.Space))
      {
        // the player is holding the key down
      }
      else if (!keyboardState.IsKeyDown(Keys.Space) && oldKeyboardState.IsKeyDown(Keys.Space))
      {
        // the player was holding the key down, but has just let it go
        if (pause)
          unpauseGame();
        else
          pauseGame();
      }


      oldKeyboardState = keyboardState;

      if (pause)
        return;

      base.Update(gameTime, keyboardState, mouseState, gamePadState);

      updateRemainingRoundTime(gameTime);

      
      

      if (keyboardState.IsKeyDown(Keys.F4) && !f4KeyHandled)
      {
        f4KeyHandled = true;

        if (pointBarTest.CurrentPoints >= 5)
          pointBarTest.CurrentPoints -= 5;
        else
          pointBarTest.CurrentPoints = pointBarTest.MaxPoints;

      }

      if (keyboardState.IsKeyUp(Keys.F4))
        f4KeyHandled = false;

      if (pointBarTest.CurrentPoints < (pointBarTest.MaxPoints * 0.25f))
        pointBarTest.BarColor = Color.Red;
      else
        pointBarTest.BarColor = Color.Lime;

      if (mouseState.LeftButton == ButtonState.Pressed && !leftMousePressed)
      {
        leftMousePressed = true;
        //begin selection
        selectionRectangle.Position = new Vector2(mouseState.X, mouseState.Y);
        selectionRectangle.Size = new Vector2(1, 1);
        selectionStart = mouseState.ToPoint();

        if (!Scene.Contains(selectionRectangle))
          Scene.Add(selectionRectangle);
      }
      else if (mouseState.LeftButton == ButtonState.Pressed && leftMousePressed)
      {
        selectionRectangle.Position = new Vector2(
          Math.Min(selectionStart.X, mouseState.X),
          Math.Min(selectionStart.Y, mouseState.Y)
          );

        int sizeX = 0;
        int sizeY = 0;
        if (mouseState.X >= selectionStart.X)
          sizeX = mouseState.X-selectionStart.X;
        else
          sizeX = selectionStart.X - mouseState.X;

        if (mouseState.Y >= selectionStart.Y)
          sizeY = mouseState.Y-selectionStart.Y;
        else
          sizeY = selectionStart.Y - mouseState.Y;


        selectionRectangle.Size = new Vector2(sizeX, sizeY);
      }
      
      if (mouseState.LeftButton == ButtonState.Released && leftMousePressed)
      {
        if (Scene.Contains(selectionRectangle))
          Scene.Remove(selectionRectangle);

        leftMousePressed = false;

        if (selectionRectangle.Rectangle.Contains(person.CenterLocation.ToPoint()))
        {
          selection = person;
          person.Coloration = Color.Lime;
          person.Texture = contentProvider.GetTextureBundle("human_player_highlight").Texture2D;
        }
        else
        {
          selection = null;
          person.Coloration = Color.White;
          person.Texture = contentProvider.GetTextureBundle("human_player_yellow").Texture2D;
        }
      }
      
      foreach (Zombie zombie in zombies)
      {
        if (!zombie.IsMoving)
        {
          zombie.MoveTo(person.CenterLocation);
        }
        else
        {
          var modifier = zombie.GetModifierOfType<LinearMovementModifier>();

          if (modifier != null && modifier.Destination != person.CenterLocation)
          {
            zombie.ClearModifier();
            zombie.MoveTo(person.CenterLocation);
          }
        }
      }

      if (keyboardState.IsKeyDown(Keys.F5) && !f5KeyHandled)
      {
        f5KeyHandled = true;
        createZombies(5);
      }    

      if (keyboardState.IsKeyUp(Keys.F5) && f5KeyHandled)
      {
        f5KeyHandled = false;
      }

      //if (!LastAttack.HasValue)
      //  LastAttack = gameTime.TotalGameTime;
      //else if ((gameTime.TotalGameTime - LastAttack.Value) > AttackIntervall)
      //{ 
      //  //ATTACK!!!!
      //  createZombies(5);
      //}

      //if (selection == null)
      //{
      //  if (person.IsHitBy(mouseState.X, mouseState.Y))
      //  {
      //    person.Texture = contentProvider.GetTextureBundle("human_player_highlight").Texture2D;

      //    if (mouseState.LeftButton == ButtonState.Pressed)
      //    {
      //      selection = person;
      //      person.Color = Color.Lime;
      //    }
      //  }
      //  else if (mouseState.LeftButton == ButtonState.Pressed)
      //  {
      //    foreach (Zombie zombie in zombies)
      //    {
      //      if (zombie.IsHitBy(mouseState))
      //        zombie.HitPoints -= 10;
      //    }
      //  }
      //  else
      //  {
      //    person.Texture = contentProvider.GetTextureBundle("human_player_yellow").Texture2D;
      //  }
      //}
      //else
      //{
      //  if (mouseState.LeftButton == ButtonState.Pressed && !person.IsHitBy(mouseState))
      //  {
      //    selection.Color = Color.White;
      //    selection = null;
      //  }

      handleRightClick(mouseState);
      //}

    }

    private void handleRightClick(MouseState mouseState)
    {
      if (selection == null)
        return;

      if (mouseState.RightButton == ButtonState.Pressed)
      {
        if (person.HasModifierOfType<LinearMovementModifier>())
          person.ClearModifier();

        foreach (Feld feld in spielfeld)
        {
          if (feld.Mitte.IsHitBy(mouseState))
          {
            person.MoveTo(feld.Mitte.CenterLocation);
            break;
          }
        }
      }
    }

    private void updateRemainingRoundTime(GameTime gameTime)
    {
      remainingRoundTime = remainingRoundTime - gameTime.ElapsedGameTime;

      labelRemainingRoundTime.Text = remainingRoundTime.ToString(@"mm\:ss");
    }

    private void pauseGame()
    {
      pause = true;
      labelPause.Visible = true;

    }

    private void unpauseGame()
    {
      pause = false;
      labelPause.Visible = false;
    }

    private void letZombieDie(Zombie zombie)
    {      
      zombie.ClearModifier();
      zombie.Die();
    }

    private List<Zombie> zombies = new List<Zombie>();

    private void createZombies(int count)
    {
      for (int i = 0; i < 5; i++)
      {
        Zombie zombie = new Zombie();

        zombie.SetTextureBundle(Global.ContentProvider.GetTextureBundle("rock_enemy_red"));

        zombie.Initialize();

        zombie.CenterLocation = spielfeld[LazyRandom.NextInt(10), 0].Mitte.CenterLocation;

        zombies.Add(zombie);

        Scene.Add(zombie);
      }
    }
  }
}
