﻿using LazyMonoLib.Modifiers;
using LazyMonoLib.EventArgs;
using LazyMonoLib.EventHandler;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib
{
  public class Sprite_old //: ISprite
  {
    private bool visible = true;
    private Vector2 location = new Vector2(0f, 0f);
    private Vector2 size = new Vector2(1f, 1f);



    private Color coloration = Color.White;
    
    
    protected Rectangle? sourceRectangle = null;
    private List<Rectangle> rectangles = new List<Rectangle>();
    private List<IModifier> modifiers = new List<IModifier>();
    private Rotation rotation = null;
    private bool deleteMe = false;

    public Sprite_old()
    { }

    public string Name { get; set; }

    public void SetSourceRectangle(Rectangle? sourceRectangle)
    {
      this.sourceRectangle = sourceRectangle;
    }

    public bool DeleteMe { get { return deleteMe; } set { deleteMe = value; } }

    public object Tag { get; set; }

    public Color Coloration
    {
      get { return coloration; }
      set
      {
        if (value != coloration)
        {
          coloration = value;
          colorationChanged();
        }        
      }
    }

    public Rotation Rotation { get { return rotation; } set { rotation = value; } }

    public K TagAs<K>()
    {
      if (Tag != null && Tag is K)
        return (K)Tag;
      else
        return default(K);
    }

    public Texture2D Texture { get; set; }

    public Dictionary<string, SubtextureDefinition> Subtextures { get; private set; }

    public void SetTextureBundle(TextureBundle bundle)
    {
      Texture = bundle.Texture2D;
      Subtextures = bundle.Subtextures;
    }

    public Vector2 Position { get { return location; } set { location = value; } }

    public Vector2 CenterLocation
    {
      get
      {
        return new Vector2(
          Position.X + (Size.X / 2f),
          Position.Y + (Size.Y / 2f));
      }
      set
      {
        location = new Vector2(
          value.X - (Size.X / 2f),
          value.Y - (Size.Y / 2f));
      }
    }

    public Vector2 Size
    {
      get { return size; }
      set { size = value; }
    }

    public List<Rectangle> Rectangles { get { return rectangles; } set { rectangles = value; } }

    public bool Visible { get { return visible; } set { visible = value; } }

    public virtual void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      if (MouseOver != null && !isMouseOver && IsHitBy(mouseState))
      {
        isMouseOver = true;
        MouseOver(this, mouseState);
      }
      else if (MouseLeave != null && isMouseOver && !IsHitBy(mouseState))
      {
        isMouseOver = false;
        MouseLeave(this, mouseState);
      }

      if (mouseState.LeftButton == ButtonState.Pressed && IsHitBy(mouseState) && MouseClick != null)
        MouseClick(this, mouseState);

      processAnimations(gameTime);
    }

    protected void processAnimations(GameTime gameTime)
    {
      for (int i = 0; i < modifiers.Count; i++)
      {
        if (modifiers[i] == null)
          continue;

        modifiers[i].NextStep(this, gameTime);

        if (modifiers[i].IsFinished)
          modifiers.RemoveAt(i);
      }
    }

    protected virtual void colorationChanged()
    { }

    public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      if (!Visible)
        return;

      spriteBatch.Draw(Texture, getRectangle(), sourceRectangle, coloration);
    }

    private Rectangle getRectangle()
    {
      return new Rectangle((int)location.X, (int)location.Y, (int)Size.X, (int)Size.Y);
    }

    public void AddModifier(IModifier modifier)
    {
      modifiers.Add(modifier);
    }

    public void RemoveModifier(IModifier modifier)
    {
      if (modifiers.Contains(modifier))
        modifiers.Remove(modifier);
    }

    public void ClearModifier()
    {
      modifiers.Clear();
    }

    public bool HasModifier(IModifier modifier)
    {
      return modifiers.Contains(modifier);
    }

    public bool HasModifierOfType<K>()
    {
      foreach (IModifier modifier in modifiers)
      {
        if (modifier is K)
          return true;
      }
      return false;
    }

    public K GetModifierOfType<K>()
    {
      foreach (IModifier modifier in modifiers)
      {
        if (modifier is K)
          return (K)modifier;
      }

      return default(K);
    }

    public int ModifierCount
    {
      get
      {
        if (modifiers != null)
          return modifiers.Count;
        else 
          return 0;
      }
    }

    public override string ToString()
    {
      return "Sprite";
    }

    public bool IsHitBy(Point point)
    {
      return IsHitBy(point.X, point.Y);
    }

    public bool IsHitBy(Vector2 vector)
    {
      return IsHitBy((int)vector.X, (int)vector.Y);
    }

    public bool IsHitBy(int x, int y)
    {
      return
        x >= location.X &&
        x <= location.X + size.X &&
        y >= location.Y &&
        y <= location.Y + size.Y;
    }

    public bool IsHitBy(MouseState mouseState)
    {
      return IsHitBy(mouseState.X, mouseState.Y);
    }

    public event MouseEventHandler MouseOver;

    public event MouseEventHandler MouseLeave;

    private bool isMouseOver = false;

    public event MouseEventHandler MouseClick;

    public virtual void OnMouseOver()
    { }

    public virtual void OnMouseClick(MouseEventArgs_old mouseArgs)
    { }


    public void SetToSubtextureNamed(string subtextureName)
    {
      SetSourceRectangle(this.Subtextures[subtextureName].Rectangle);
    }
  }
}
