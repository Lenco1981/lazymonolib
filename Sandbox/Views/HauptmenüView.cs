﻿using LazyMonoLib;
using LazyMonoLib.Controls;
using LazyMonoLib.Primitives;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Sandbox.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Views
{
  public class HauptmenüView : BaseView<HauptmenüController>
  {
    private Sprite background;
    private Sprite quitButton;
    private Sprite playButton;
    private Sprite logo;

    private Label labelBeta;

    public HauptmenüView(HauptmenüController controller)
      : base(controller)
    { }

    public override void InitializeUI()
    {
      labelBeta = new Label();
      labelBeta.Color = Color.Red;
      labelBeta.TextColor = Color.Red;
      labelBeta.Font = Global.ContentProvider.GetFont("calibri", 12f);
      labelBeta.Text = "ZOMBIE SURVIAL - v0.0.1";
      labelBeta.CenterLocation = new Vector3(Global.ScreenWidth / 2f, Global.ScreenHeight - 20f, 0f);     

      UI.Add(labelBeta);
    }

    public override void InitializeScene()
    {
      logo = new Sprite();
      logo.Texture = Global.ContentProvider.GetTexture2D("title_screen_logo");
      logo.Size = new Vector2(400, 400);
      logo.CenterLocation = new Vector2(Global.ScreenWidth / 2, 180);
      logo.Coloration = Color.White;

      playButton = new Sprite();
      playButton.Name = "PlayButton";
      playButton.SetTextureBundle(Global.ContentProvider.GetTextureBundle("button_play"));
      playButton.Size = new Vector2(256, 64);
      playButton.Coloration = Color.White;
      playButton.SetSourceRectangle(playButton.Subtextures["normal"].Rectangle);
      playButton.MouseOver += playButton_MouseOver;
      playButton.MouseLeave += playButton_MouseLeave;
      playButton.CenterLocation = new Vector2(Global.ScreenWidth / 2, 450);
      playButton.MouseClick += playButton_MouseClick;

      quitButton = new Sprite();
      quitButton.SetTextureBundle(Global.ContentProvider.GetTextureBundle("button_quit"));
      quitButton.Size = new Vector2(256, 64);
      quitButton.Coloration = Color.White;
      quitButton.SetSourceRectangle(playButton.Subtextures["normal"].Rectangle);
      quitButton.MouseOver += playButton_MouseOver;
      quitButton.MouseLeave += playButton_MouseLeave;
      quitButton.CenterLocation = new Vector2(Global.ScreenWidth / 2, 550);
      quitButton.MouseClick += quitButton_MouseClick;
      quitButton.Name = "QuitButton";

      Scene.Add(logo);
      Scene.Add(playButton);
      Scene.Add(quitButton);
    }

    void quitButton_MouseClick(ISprite sender, Microsoft.Xna.Framework.Input.MouseState mouseState)
    {
      Global.Game.Exit();
    }

    void playButton_MouseClick(ISprite sender, Microsoft.Xna.Framework.Input.MouseState mouseState)
    {
      Global.Game.SetCurrentController(new MainController());
    }

    void playButton_MouseLeave(ISprite sender, Microsoft.Xna.Framework.Input.MouseState mouseState)
    {
      sender.SetSourceRectangle(playButton.Subtextures["normal"].Rectangle);
    }

    void playButton_MouseOver(ISprite sender, Microsoft.Xna.Framework.Input.MouseState mouseState)
    {
      sender.SetToSubtextureNamed("hover");
    }

    public override void InitializeBackground()
    {
      background = new Sprite();
      background.Texture = Global.ContentProvider.GetTexture2D("title_screen_background");
      background.Position = new Microsoft.Xna.Framework.Vector2(0, 0);
      background.Size = Global.ScreenSize;

      Background.Add(background);
    }
  }
}
