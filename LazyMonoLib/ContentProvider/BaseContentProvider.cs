﻿using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Enums;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;


namespace LazyMonoLib.ContentProvider
{
	public abstract class BaseContentProvider
	{
		public string ContentRootFolder { get; private set; }

		public string FontsSubfolder = @"Fonts\";
		public string TexturesSubfolder = @"Textures\";
		public string LanguagesSubfolder = @"Languages\";

		protected ContentManager content;

		protected Dictionary<string, TextureBundle> textures = new Dictionary<string, TextureBundle>();
		protected Dictionary<string, Dictionary<float, SpriteFont>> fonts = new Dictionary<string, Dictionary<float, SpriteFont>>();

		public BaseContentProvider(ContentManager manager)
		{
			content = manager;
			ContentRootFolder = content.RootDirectory;
		}

		public virtual TextureBundle GetTextureBundle(string name)
		{
			return textures[name];
		}

		public virtual Texture2D GetTexture2D(string name)
		{
			return textures[name].Texture2D;
		}

		public virtual SpriteFont GetFont(string name, float size)
		{
			if (fonts.ContainsKey(name) && fonts[name].ContainsKey(size))
				return fonts[name][size];
			else
				throw new KeyNotFoundException("Can't find font named '" + name + "' with size '" + size.ToString() + "' in Content/Fonts");
		}

		/// <summary>
		/// Loads a texture into the textures dictionary
		/// </summary>
		protected void loadTextureFromFile(string textureFile)
		{
			string contentFilename = convertFilepathToContentTarget(textureFile);
			string xmlFilename = convertFilepathToXmlTarget(textureFile);
			string textureName = getTextureNameFromFilename(textureFile);

			Texture2D texture = content.Load<Texture2D>(contentFilename);

			SubtextureCollection subTextures = new SubtextureCollection();

			try
			{
				if (File.Exists(xmlFilename))
				{
					subTextures = SubtextureCollection.LoadFromXML(xmlFilename);
				}
			}
			catch { }

			textures.Add(textureName, new TextureBundle(texture, subTextures.Values));
		}

		/// <summary>
		/// Loads a spritefont into the fonts dictionary
		/// </summary>
		protected void loadFontFromFile(string fontFile)
		{
			string contentFilename = convertFilepathToContentTarget(fontFile);
			string fontName = getFontNameFromFile(fontFile);
			float fontSize = getFontSizeFromFile(fontFile);

			if (!fonts.ContainsKey(fontName))
				fonts.Add(fontName, new Dictionary<float, SpriteFont>());

			fonts[fontName].Add(fontSize, content.Load<SpriteFont>(contentFilename));
		}

		protected void loadLanguageFromFile(string languageFile)
		{ 
			
		}

		internal string getFontNameFromFile(string fontFile)
		{
			return fontFile.PartAfterLast("\\", ReturnValueOnNotFound.ReturnEntireString)
				.PartBeforeLast("_", ReturnValueOnNotFound.ReturnEntireString);
		}

		internal float getFontSizeFromFile(string fontFile)
		{
			string sizeString = fontFile.PartAfterLast("_", ReturnValueOnNotFound.ReturnEmptyString).PartBeforeFirst(".xnb", false);

			float result = 0f;

			if (float.TryParse(sizeString, out result))
			{
				return result;
			}
			else
				throw new InvalidDataException("Can't extract font size from '" + fontFile + "'");
		}

		internal string convertFilepathToContentTarget(string fullFilePath)
		{
			string target = "";

			if (fullFilePath.StartsWith("Content"))
			{
				target = fullFilePath.Substring(8);

			}
			else
				target = fullFilePath;

			if (target.EndsWith(".xnb"))
			{
				return target.Substring(0, target.Length - 4);
			}
			else
				return target;
		}

		internal string convertFilepathToXmlTarget(string file)
		{
			string target = "";

			if (file.EndsWith(".xnb"))
				target = file.Substring(0, file.Length - 4) + ".xml";

			return target;
		}

		internal string getTextureNameFromFilename(string file)
		{
			return Path.GetFileNameWithoutExtension(file.PartAfterLast("\\", ReturnValueOnNotFound.ReturnEntireString));
		}
	}
}
