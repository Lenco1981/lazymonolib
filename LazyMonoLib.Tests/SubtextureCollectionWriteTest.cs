﻿using LazyMonoLib.ValueObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Tests
{
  public class SubtextureCollectionWriteTest
  {
    [Test]
    public void WriteSubtextureCollectionToFileTest()
    {
      string filename = @"D:\collection.xml";

      SubtextureCollection collection = new SubtextureCollection();

      collection.Values.Add(new SubtextureDefinition { Name = "walking01", X = 0, Y = 0, Width = 32, Height = 32 });
      collection.Values.Add(new SubtextureDefinition { Name = "walking02", X = 5, Y = 32, Width = 32, Height = 32 });
      collection.Values.Add(new SubtextureDefinition { Name = "walking03", X = 10, Y = 64, Width = 32, Height = 32 });
      collection.Values.Add(new SubtextureDefinition { Name = "walking04", X = 15, Y = 96, Width = 32, Height = 32 });

      SubtextureCollection.WriteToXML(filename, collection);

      Assert.That(System.IO.File.Exists(filename));

      SubtextureCollection test = SubtextureCollection.LoadFromXML(filename);

      Assert.That(test.Values.Count, Is.EqualTo(4));
    }

  }
}
