﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Fades in the sprite by changing the coloration alpha channel value over time
  /// </summary>
  public class FadeInAnimation : SpriteAnimation
  {
    private TimeSpan duration;

    private double stepsize = 1f;

    private double alpha = 0f;

    private double startAlpha = 0f;

    private double endAlpha = 0f;

    public FadeInAnimation(TimeSpan duration)
      : this(duration, 0, 255)
    { }

    public FadeInAnimation(int seconds)
      : this(new TimeSpan(0, 0, seconds))
    { }

    public FadeInAnimation(TimeSpan duration, double startAlpha, double endAlpha)
    {
      this.duration = duration;
      this.startAlpha = startAlpha;
      this.endAlpha = endAlpha;

      stepsize = (endAlpha - startAlpha) / (double)duration.TotalMilliseconds;
    }

    public override void NextStep(ISprite sprite, GameTime gameTime, ICamera camera)
    {
        alpha = alpha + (stepsize * gameTime.ElapsedGameTime.TotalMilliseconds);
        sprite.Coloration = new Color(sprite.Coloration, (int)alpha);      
    }

    public override bool IsFinished
    {
      get
      {
        return alpha >= endAlpha;
      }
    }
  }
}
