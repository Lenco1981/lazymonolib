﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LazyMonogameLib.Project;

namespace LazyMonogameEditorWinForms
{
	public partial class FrmMain : Form
	{
		private MonogameProject project { get; set; }

		public FrmMain()
		{
			InitializeComponent();
		}

		

		private void button1_Click(object sender, EventArgs e)
		{
			project = new MonogameProject();
			project.FullProjectPath = textBox1.Text;
			project.FillFromPath();

			treeView1.Nodes.Clear();
			treeView1.Nodes.Add(generateNode(project.Root));
		}

		private TreeNode generateNode(IProjectEntry entry)
		{
			TreeNode node = new TreeNode();
			node.Text = entry.DisplayName;
			node.Tag = entry;

			entry.Childs.ForEach(x => node.Nodes.Add(generateNode(x)));

			return node;
		}


	}
}
