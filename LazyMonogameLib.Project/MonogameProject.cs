﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyMonogameLib.Project
{
	public class MonogameProject
	{
		public string FullProjectPath { get; set; }

		public string ProjectName { get; set; }

		public IProjectEntry Root { get; set; }

		public void FillFromPath()
		{
			Root = ProjectEntryFactory.CreateFromPath(FullProjectPath);
		}
	}
}
