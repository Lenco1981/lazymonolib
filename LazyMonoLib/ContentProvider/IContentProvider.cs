﻿using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.ContentProvider
{
  public interface IContentProvider
  {
    Texture2D GetTexture2D(string name);

    TextureBundle GetTextureBundle(string name);

    SpriteFont GetFont(string name, float size);

    void LoadContent();

    //void AddXmlFilePath(string xmlFile);
  }
}
