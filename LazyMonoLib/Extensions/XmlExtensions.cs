﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using System.Globalization;
using System.Drawing;

using System.Xml.Linq;
using LazyMonoLib.Serializer;

namespace System.Xml.Linq
{
  public static class XmlExtensions
  {
    private static ColorConverter ColorConverter = new ColorConverter();

    public static bool HasAttribute(this XElement element, string attributeName)
    {
      foreach (XAttribute attribute in element.Attributes())
      {
        if (attribute.Name == attributeName)
          return true;
      }
      return false;
    }

    public static XAttribute GetAttributeByName(this XElement element, string attributeName)
    {
      foreach (XAttribute attribute in element.Attributes())
      {
        if (attribute.Name == attributeName)
          return attribute;
      }
      return null;
    }

    public static Color AttributeValueAsColor(this XElement element, string attributeName)
    {
      foreach (XAttribute attribute in element.Attributes())
      {
        if (attribute.Name == attributeName)
        {
          return (Color)ColorConverter.ConvertFromString(attribute.Value);
        }
      }
      throw new InvalidOperationException("Can't find " + attributeName);
    }

    public static string GetNameFromNameAttribute(this XElement element)
    {
      XAttribute nameAttribute = GetAttributeByName(element, XMLSerializer.XML_NAME);

      if (nameAttribute == null)
        throw new NullReferenceException("Attribute named 'Name' could not be found - invalid XmlElement!");

      return nameAttribute.Value;
    }

    public static float GetProbabilityFromProbabilityAttribute(this XElement element)
    {
      return AttributeValueAsFloat(element, XMLSerializer.XML_PROBABILITY, 0f);
    }

    public static string GetAttributeValueSafelyByName(this XElement element, string attributeName, string valueIfError = "")
    {
      foreach (XAttribute attribute in element.Attributes())
      {
        if (attribute.Name == attributeName)
          return attribute.Value;
      }
      return valueIfError;
    }

    public static String AttributeValueAsString(this XElement element, string attributeName)
    {
      return AttributeValueAsString(element, attributeName, String.Empty);
    }

    public static String AttributeValueAsString(this XElement element, string attributeName, string valueIfEmpty)
    {
      XAttribute attribute = GetAttributeByName(element, attributeName);

      if (attribute != null)
        return attribute.Value;
      else
        return valueIfEmpty;
    }

    public static int AttributeValueAsInteger(this XElement element, string attributeName, int valueIfEmpty)
    {
      XAttribute attribute = GetAttributeByName(element, attributeName);

      if (attribute != null)

        return attribute.Value.ToIntegerSafely(valueIfEmpty);
      else
        return valueIfEmpty;
    }

    public static float AttributeValueAsFloat(this XElement element, string attributeName, float valueIfEmpty)
    {
      XAttribute attribute = GetAttributeByName(element, attributeName);

      if (attribute != null)
      {
        float result = valueIfEmpty;

        float.TryParse(attribute.Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result);

        return result;
      }
      else
        return valueIfEmpty;
    }

    public static float AttributeValueAsFloat(this XElement element, string attributeName)
    {
      XAttribute attribute = GetAttributeByName(element, attributeName);

      if (attribute != null)
      {
        float result = 0f;

        if (!float.TryParse(attribute.Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result))
          throw new InvalidCastException("Can't parse float value from attribute value");

        return result;
      }
      else
        throw new InvalidOperationException("Can't find attribute " + attributeName);
    }

    public static decimal AttributeValueAsDecimal(this XElement element, string attributeName, decimal valueIfEmpty)
    {
      XAttribute attribute = GetAttributeByName(element, attributeName);

      if (attribute != null)
      {
        decimal result = valueIfEmpty;

        decimal.TryParse(attribute.Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result);

        return result;
      }
      else
        return valueIfEmpty;
    }

    public static IList<String> AttributeValueAsStringList(this XElement element, string attributeName)
    {
      XAttribute attribute = GetAttributeByName(element, attributeName);

      if (attribute != null)
        return attribute.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
      else
        return new List<String>();
    }

    public static IList<String> ElementValueAsStringList(this XElement element)
    {
      return TrimList(element.Value.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList());
    }

    private static List<String> TrimList(List<String> listToTrim)
    {
      return listToTrim.ConvertAll(x => x.TrimSafely());
    }

    public static XElement GetChildElementByName(this XElement element, string name)
    {
      foreach (XElement child in element.Descendants())
      {
        if (child.Name == name)
          return child;
      }
      throw new InvalidOperationException("Can't find any child element namend " + name);
    }

    public static void AddAttribute(this XElement element, string name, object value)
    {
      element.Add(new XAttribute(XName.Get(name), value));
    }
  }
}
