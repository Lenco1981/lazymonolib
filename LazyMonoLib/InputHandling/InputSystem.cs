﻿using LazyMonoLib.EventArgs;
using LazyMonoLib.EventHandler;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.InputHandling
{
  public class InputSystem : Interfaces.IUpdateable
  {
    public InputSystem(object sender)
    {
      this.sender = sender;
    }

    private object sender;

    private KeyboardState lastKeyboardState = new KeyboardState();
    private MouseState lastMouseState = new MouseState();
    private GamePadState lastGamePadState = new GamePadState();

    public event MouseEventHandler MouseDown;
    public event MouseEventHandler MouseUp;
    public event MouseEventHandler MouseClick;

    public event KeyboardEventHandler KeyDown;
    public event KeyboardEventHandler KeyUp;
    public event KeyboardEventHandler KeyPress;

    public void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      if (keyboardState.GetPressedKeys() != lastKeyboardState.GetPressedKeys())
      { }
    }

    private void fireKeyPress(KeyboardState keyboardState)
    {
      if (KeyPress != null)
        KeyPress(null, new KeyEventArgs(keyboardState));
    }
  }
}
