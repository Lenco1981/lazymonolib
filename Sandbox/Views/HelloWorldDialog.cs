﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib;
using LazyMonoLib.Controls;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Sandbox.Views
{
  public class HelloWorldDialog : AbstractDialog
  {
    private Label labelHello;

    private FrameBackground frameBackground;

    public HelloWorldDialog()
      : base()
    {
      this.Size = new Microsoft.Xna.Framework.Vector2(400, 150);

      Initialize();
    }


    public override void InitializeUI()
    {
      labelHello = new Label();
      labelHello.Font = Global.ContentProvider.GetFont("calibri", 14f);
      labelHello.Position = new Vector2(10, 10);
      labelHello.Text = "Hello World!";

      UI.Add(labelHello);
    }

    public override void InitializeScene()
    {
      
    }

    public override void InitializeBackground()
    {
      frameBackground = new FrameBackground("background", "message_box");
      frameBackground.Size = this.Size;

      Background.Add(frameBackground);
    }

    private KeyboardState oldState = new KeyboardState();

    protected override void updateView(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      if (oldState.IsKeyDown(Keys.Enter) && keyboardState.IsKeyUp(Keys.Enter))
      {
        this.DialogResult = LazyMonoLib.Enums.DialogResult.OK;
      }
      else if (oldState.IsKeyDown(Keys.Delete) && keyboardState.IsKeyUp(Keys.Delete))
        this.DialogResult = LazyMonoLib.Enums.DialogResult.Cancel;

      oldState = keyboardState;
    }
  }
}
