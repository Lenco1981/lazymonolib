﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Performs a cyclic movement between departure and destination points using the given speed.
  /// Moving the sprite back and forth
  /// </summary>
  public class CycleMovementModifier : SpriteAnimation
  {
    public Vector2 Destination { get; private set; }

    public Vector2 Departure { get; private set; }

    public TimeSpan Duration { get; private set; }  

    private Vector2 stepSize = Vector2.Zero;

    private TimeSpan durationCopy;

    public CycleMovementModifier(Vector2 departure, Vector2 destination, TimeSpan duration)
    {
      Departure = departure;
      Duration = duration;
      Destination = destination;

      durationCopy = Duration;

      initializeStepSize();
    
    }

    private void initializeStepSize()
    {
      stepSize.X = (Destination.X - Departure.X) / (float)Duration.TotalMilliseconds;
      stepSize.Y = (Destination.Y - Departure.Y) / (float)Duration.TotalMilliseconds;
    }

    public override void NextStep(ISprite sprite, GameTime gameTime, ICamera camera)
    {
      if (durationCopy.Ticks > 0) //Perform animation!
      {
        sprite.CenterPosition += (stepSize * (float)gameTime.ElapsedGameTime.TotalMilliseconds);
        durationCopy -= gameTime.ElapsedGameTime;
      }
      else //animation finished, reset and turn around!
      {
        durationCopy = Duration;
        stepSize = Vector2.Negate(stepSize);
      }

    }

    public override bool IsFinished
    {
      get { return false; }
    }
  }
}
