﻿using LazyMonoLib.Interfaces;
using LazyMonoLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.ContentProvider;

namespace LazyMonoLib.Controller
{
  public abstract class BaseController<VIEW_TYPE> : IController
    where VIEW_TYPE : IView
  {
    //public BaseController(VIEW_TYPE view)
    //{
    //  View = view;

    //  View.InitializeBackground();

    //  View.InitializeScene();

    //  View.InitializeUI();
    //}

    public VIEW_TYPE View { get; protected set; }

    public abstract void OnGetActive();

    public abstract void OnGetInactive();

    public abstract bool CanGetInactive();

    /// <summary>
    /// Explicit implementation to avoid generic parameters in IController
    /// </summary>
    IView IController.View
    {
      get { return View; }
    }
  }
}
