﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;


namespace LazyMonoLib.Serializer
{
  /// <summary>
  /// Contains a collection of elements each with a probability to be returned on GetRandomElement
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class XmlProbabilityDistribution<T> : XmlEnum<T>
    where T : XmlProbabilityEntry, new()
  {
    public XmlProbabilityDistribution()
      : base()
    { }

    public XmlProbabilityDistribution(XElement rootElement)
      : base(rootElement)
    {
      root = rootElement;
    }


    public override T GetRandomElement()
    {
      float value = LazyRandom.NextFloat();

      float sum = 0f;

      foreach (T entry in Values)
      {
        sum += entry.Probability;

        if (value <= sum)
          return entry;        
      }

      return Values.Last();
    }

    protected override List<T> loadFromXml()
    {
      List<T> list = new List<T>();

      float sum = 0f;

      foreach (XElement child in root.Descendants(XName.Get(xmlElementName, String.Empty)))
      {
        T entry = createEntry(child);
        loadAdditionalProperites(ref entry, child);
        sum += entry.Probability;
        list.Add(entry);
      }

      if (sum != 1f)
        throw new InvalidOperationException("The probability sum was not equal to 1");

      return list;    
    }

    protected override T createEntry(XElement element)
    {
      T entry = new T();

      entry.Name = element.GetNameFromNameAttribute();
      entry.Probability = element.GetProbabilityFromProbabilityAttribute();
      return entry;
    }
  }
}
