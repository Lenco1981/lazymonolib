﻿using LazyMonoLib.Enums;
using LazyMonoLib.ValueObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Tests
{

  public class RadianDirectionTests
  {
    [Test]
    public void ConvertToCompass16Test()
    {
      double PI_180 = Math.PI / 180;

      CompassSixteenth result = RadianDirection.GetCompass16DirectionToRadian(0d);

      Assert.That(result, Is.EqualTo(CompassSixteenth.East));
    

    }
  }
}
