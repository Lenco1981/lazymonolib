﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace LazyMonoLib.Serializer
{
  public static class XMLSerializer
  {
    private const string XML_FILTER = "*.xml";

    public static string XML_NAME = "Name";
    public static string XML_VALUE = "Value";
    public static string XML_PROBABILITY = "Probability";

    public static string XML_FOLDER_CONFIG = "";

    private static List<XElement> elements = null;
    public static List<XElement> Elements
    {
      get
      {
        if (elements == null)
        {
          elements = new List<XElement>();
          foreach (string xmlFile in Directory.GetFiles(XML_FOLDER_CONFIG, XML_FILTER))
          {
            elements.Add(LoadFromFile(xmlFile));
          }
        }
        return elements;
      }
    }

    private static XElement LoadFromFile(string filename)
    {
      return XElement.Load(filename);
    }

    public static XElement GetElementByName(string name)
    {
      XElement result = null;
      foreach (XElement element in Elements)
      {
        IEnumerable<XElement> possibleResults = element.Descendants(XName.Get(name, String.Empty));
        if (possibleResults.Count() >= 1) //geändert! vorher stand da ==1
        {
          result = possibleResults.First();
          break;
        }
      }
      return result;
    }

    public static void SaveToFile<T>(XmlEnum<T> ymlEnum)
      where T : XmlEnumEntry, new()
    {
      string file = Path.Combine(XML_FOLDER_CONFIG, ymlEnum.Filename);

      XElement root = new XElement(XName.Get("DeepSpace"));

      root.Add(ymlEnum.WriteToXmlElement());

      root.Save(file);
    }



    public static T CreateFromXElement<T>(XElement element) where T : ICreateableFromXElement, new()
    {
      T obj = new T();

      obj.CreateFromXElement(element);

      return obj;
    }

    public static T CreateByPrototypeFromXElement<T>(T prototype, XElement element) where T : ICreateableFromXElement
    {
      prototype.CreateFromXElement(element);
      return prototype;
    }
  }
}