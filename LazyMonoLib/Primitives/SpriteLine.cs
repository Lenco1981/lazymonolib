﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LazyMonoLib.Primitives
{
  public class SpriteLine : Entity, IEntity
  {
    public Vector2 Start { get; set; }
    public Vector2 End { get; set; }

    private Texture2D texture;

    public SpriteLine()
    {
      texture = new Texture2D(Global.GraphicsDevice, 1, 1);
      texture.SetData<Color>(
          new Color[] { Color.White });// fill the texture with white
    }

    public Texture2D Texture { get { return texture; } set { texture = value; } }

    public int LineWidth { get; set; }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      Vector2 edge = camera.Transform(End) - camera.Transform(Start);
      // calculate angle to rotate line
      float angle =
          (float)Math.Atan2(edge.Y, edge.X);


      spriteBatch.Draw(texture,
          new Rectangle(// rectangle defines shape of line and position of start of line
              (int)camera.Transform(Start).X,
              (int)camera.Transform(Start).Y,
              (int)edge.Length(), //sb will strech the texture to fill this rectangle
              LineWidth), //width of line, change this to make thicker line
          null,
          Coloration, //colour of line
          angle,     //angle of line (calulated above)
          new Vector2(0, 0), // point in line about which to rotate
          SpriteEffects.None,
          0);

    }

    public override void Update(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState, Utilities.ICamera camera)
    {
      
    }
  }
}
