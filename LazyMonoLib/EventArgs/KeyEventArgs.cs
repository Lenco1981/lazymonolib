﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.EventArgs
{
  public class KeyEventArgs : EventArgs
  {
    public KeyboardState KeyboardState { get; private set; }

    public KeyEventArgs(KeyboardState keyboardState)
    {
      this.KeyboardState = keyboardState;
    }
  }
}
