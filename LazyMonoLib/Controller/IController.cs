﻿using LazyMonoLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Controller
{
  public interface IController
  {
    IView View { get; }

    void OnGetActive();

    void OnGetInactive();

    bool CanGetInactive();
  }
}
