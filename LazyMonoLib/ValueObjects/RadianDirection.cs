﻿using LazyMonoLib.Enums;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.ValueObjects
{
  public class RadianDirection
  {
    public static readonly RadianDirection East = new RadianDirection(0d, CompassFourth.East, CompassEighth.East, CompassSixteenth.East);
    public static readonly RadianDirection EastNorthEast = new RadianDirection(22.5d * PI_180, null, null, CompassSixteenth.EastNorthEast);
    public static readonly RadianDirection NorthEast = new RadianDirection(45d * PI_180, null, CompassEighth.NorthEast, CompassSixteenth.NorthEast);
    public static readonly RadianDirection NorthNorthEast = new RadianDirection(67.5d * PI_180, null, null, CompassSixteenth.NorthNorthEast);
    public static readonly RadianDirection North = new RadianDirection(90d * PI_180, CompassFourth.North, CompassEighth.North, CompassSixteenth.North);
    public static readonly RadianDirection NorthNorthWest = new RadianDirection(112.5d * PI_180, null, null, CompassSixteenth.NorthNorthWest);
    public static readonly RadianDirection NorthWest = new RadianDirection(135d * PI_180, null, CompassEighth.NorthWest, CompassSixteenth.NorthWest);
    public static readonly RadianDirection WestNorthWest = new RadianDirection(157.5d * PI_180, null, null, CompassSixteenth.WestNorthWest);
    public static readonly RadianDirection West = new RadianDirection(180d * PI_180, CompassFourth.West, CompassEighth.West, CompassSixteenth.West);
    public static readonly RadianDirection WestSouthWest = new RadianDirection(202.5d * PI_180, null, null, CompassSixteenth.WestSouthWest);
    public static readonly RadianDirection SouthWest = new RadianDirection(225d * PI_180, null, CompassEighth.SouthWest, CompassSixteenth.SouthSouthWest);
    public static readonly RadianDirection SouthSouthWest = new RadianDirection(247.5d * PI_180, null, null, CompassSixteenth.SouthSouthWest);
    public static readonly RadianDirection South = new RadianDirection(270d * PI_180, CompassFourth.South, CompassEighth.South, CompassSixteenth.South);
    public static readonly RadianDirection SouthSouthEast = new RadianDirection(292.5d * PI_180, null, null, CompassSixteenth.SouthSouthEast);
    public static readonly RadianDirection SouthEast = new RadianDirection(315d * PI_180, null, CompassEighth.SouthEast, CompassSixteenth.SouthEast);
    public static readonly RadianDirection EastSouthEast = new RadianDirection(337.5d * PI_180, null, null, CompassSixteenth.EastSouthEast);

    private static List<RadianDirection> values = new List<RadianDirection>();

    public double Radian { get; private set; }

    public CompassSixteenth? Compass16 { get; private set; }

    public CompassEighth? Compass8 { get; private set; }

    public CompassFourth? Compass4 { get; private set; }

    private RadianDirection(double radians, CompassFourth? compass, CompassEighth? compass8, CompassSixteenth? compass16)
    {
      Radian = radians;
      Compass4 = compass;
      Compass16 = compass16;
      Compass8 = compass8;

      values.Add(this);
    }

    public static CompassSixteenth GetCompass16DirectionToRadian(double radian)
    {
      foreach (RadianDirection direction in values)
      {
        if (inLeftCloseRange(radian, direction.Radian - 11.25d, direction.Radian + 11.25d))
          return direction.Compass16.Value;
      }

      throw new ArgumentOutOfRangeException("Can't determine where " + radian + "belongs to");
      //List<RadianDirection> source = values.FindAll(x => x.Compass16.HasValue).OrderBy(x => x.Radian).ToList();

      //for (int i = 0; i < source.Count() - 1; i++)
      //{
      //  if (inLeftCloseRange(radian, source[i].Radian, source[i + 1].Radian))
      //    return source[i].Compass16.Value;
      //}

      //return CompassSixteenth.South;
    }

    public CompassEighth GetCompass8DirectionToRadian(double radian)
    {
      return CompassEighth.North;
    }

    private static bool inLeftCloseRange(double value, double min, double max)
    {
      return (value >= min && value < max);
    }

    private static double PI_180 = Math.PI / 180d;
  }
}
