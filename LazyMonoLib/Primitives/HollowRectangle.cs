﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib.Primitives
{
  /// <summary>
  /// Renders a primitive rectangle with the given size at the given position having the given border thickness and color.
  /// </summary>
  public class HollowRectangle : Entity
  {
    protected Texture2D texture = null;

    private Color borderColor = Color.White;

    public HollowRectangle()
      : this(Color.White)
    { }

    public HollowRectangle(Color borderColor)
    {
      BorderColor = borderColor;
      borderColorChanged();
    }

    public int BorderThickness { get; set; }

    public Color BorderColor
    {
      get { return borderColor; }
      set
      {
        if (borderColor != value)
        {
          borderColor = value;
          borderColorChanged();
        }
      }
    }

    protected virtual void borderColorChanged()
    {
      texture = new Texture2D(Global.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
      texture.SetData(new[] { borderColor });
    }

    /// <summary>
    /// Returns the outer rectangle defined by this hollow rectangle
    /// </summary>
    public Rectangle Rectangle { get { return new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y); } }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      // Draw top line
      spriteBatch.Draw(texture, camera.Transform(new Rectangle(Rectangle.X, Rectangle.Y, Rectangle.Width, BorderThickness)), Coloration);

      // Draw left line
      spriteBatch.Draw(texture, camera.Transform(new Rectangle(Rectangle.X, Rectangle.Y, BorderThickness, Rectangle.Height)), Coloration);

      // Draw right line
      spriteBatch.Draw(texture, camera.Transform(new Rectangle((Rectangle.X + Rectangle.Width - BorderThickness),
                                      Rectangle.Y,
                                      BorderThickness,
                                      Rectangle.Height)), Coloration);
      // Draw bottom line
      spriteBatch.Draw(texture, camera.Transform(new Rectangle(Rectangle.X,
                                      Rectangle.Y + Rectangle.Height - BorderThickness,
                                      Rectangle.Width,
                                      BorderThickness)), Coloration);
    }

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera)
    {

    }
  }
}
