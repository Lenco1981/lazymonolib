﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.ContentProvider;
using Microsoft.Xna.Framework.Graphics;

namespace LazyMonoLib.ValueObjects
{
  public class TextureBundle
  {
    public TextureBundle(Texture2D texture2D, List<SubtextureDefinition> subtextures)
    {
      Texture2D = texture2D;

      fillDictionary(subtextures);
    }

    private void fillDictionary(List<SubtextureDefinition> subtextures)
    {
      Subtextures = new Dictionary<string, SubtextureDefinition>();

      foreach (var entry in subtextures)
        Subtextures.Add(entry.Name, entry);
    }

    public Texture2D Texture2D { get; private set; }

    public Dictionary<string, SubtextureDefinition> Subtextures { get; private set; }
  }
}
