﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  public class DeleteMeModifier : IModifier
  {
    private Sprite sprite;

    public DeleteMeModifier(Sprite sprite)
    {
      this.sprite = sprite;
    }

    public void NextStep(Interfaces.ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      if (!this.sprite.DeleteMe)
        this.sprite.DeleteMe = true;
    }

    public bool IsFinished
    {
      get { return this.sprite.DeleteMe; }
    }
  }
}
