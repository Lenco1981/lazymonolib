﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Xna.Framework
{
  public static class RectangleExtensions
  {
    public static Rectangle Add(this Rectangle rectangle, Point p)
    {
      return new Rectangle(rectangle.X + p.X, rectangle.Y + p.Y, rectangle.Width, rectangle.Height);
    }

    public static Rectangle Add(this Rectangle rectangle, Vector2 v)
    {
      return new Rectangle(rectangle.X + (int)v.X, rectangle.Y + (int)v.Y, rectangle.Width, rectangle.Height);
    }
  }
}
