﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace LazyMonoLib.Serializer
{
  public abstract class XmlEnum<T> where T : XmlEnumEntry, new()
  {
    public XmlEnum()
    {
      root = XMLSerializer.GetElementByName(xmlCollectionName);
    }

    public XmlEnum(XElement rootElement)
    {
      root = rootElement;
    }

    protected List<T> values;

    protected XElement root = null;

    public List<T> Values
    {
      get
      {
        if (values == null)
        {
          values = loadFromXml();
        }
        return values;
      }
    }

    public string Filename { get { return xmlCollectionName + ".xml"; } }

    protected abstract string xmlCollectionName { get; }

    protected abstract string xmlElementName { get; }

    protected virtual List<T> loadFromXml()
    {
      List<T> list = new List<T>();

      foreach (XElement child in root.Descendants(XName.Get(xmlElementName, String.Empty)))
      {
        T entry = createEntry(child);
        loadAdditionalProperites(ref entry, child);
        list.Add(entry);
      }

      return list;
    }

    protected virtual T createEntry(XElement element)
    {
      T entry = new T();
      entry.Name = element.GetNameFromNameAttribute();
      return entry;
    }

    protected abstract void loadAdditionalProperites(ref T entry, XElement element);

    public virtual XElement WriteToXmlElement()
    {
      root = new XElement(XName.Get(xmlCollectionName));

      foreach (T item in Values)
      {
        XElement element = createXml(item);
        writeAdditionalPropertiesToXml(ref element, item);
        root.Add(element);      
      }

      return root;
    }

    protected virtual XElement createXml(T item)
    {
      XElement element = new XElement(XName.Get(xmlElementName));
      element.AddAttribute(XMLSerializer.XML_NAME, item.Name);
      return element;
    }

    protected abstract void writeAdditionalPropertiesToXml(ref XElement element, T item);

    public virtual T GetRandomElement()
    {
      float value = LazyRandom.NextFloat();

      float sum = 0f;

      float probability = 1f / Values.Count;

      for (int i = 0; i < Values.Count; i++)
      {
        T entry = Values[i];
        sum += probability;
        if (value <= sum)
          return entry;
      }

      return Values.Last();
    }

    public T GetByName(string name)
    {
      foreach (T item in Values)
      {
        if (item.Name == name)
          return item;
      }
      throw new InvalidOperationException(name + " could not be found in " + this.GetType().Name);
    }

    public bool Contains(string name)
    {
      return Values.Exists(x => x.Name == name);
    }
  }
}
