﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.ValueObjects
{
	/// <summary>
	/// Defines a named subregion in a texture file.
	/// </summary>
	public class SubtextureDefinition
	{

		/// <summary>
		/// The region name
		/// </summary>
		[XmlAttribute]
		public string Name { get; set; }

		/// <summary>
		/// The x coordinate of the region in the image.
		/// </summary>
		[XmlAttribute]
		public int X { get; set; }

		/// <summary>
		/// The y coordinate of the region in the image.
		/// </summary>
		[XmlAttribute]
		public int Y { get; set; }

		/// <summary>
		/// The width of the subtexture
		/// </summary>
		[XmlAttribute]
		public int Width { get; set; }

		/// <summary>
		/// The height of the subtexture
		/// </summary>
		[XmlAttribute]
		public int Height { get; set; }

		/// <summary>
		/// Gets a rectangle created from the x,y, width and height properties
		/// </summary>
		[XmlIgnore]
		public Rectangle Rectangle { get { return new Rectangle(X, Y, Width, Height); } }

		/// <summary>
		/// The texture name this subregion belongs to.
		/// </summary>
		[XmlAttribute]
		public string TextureName { get; set; }
	}
}
