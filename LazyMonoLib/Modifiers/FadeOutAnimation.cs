﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Fades out a sprite by changing the coloration alpha channel value over time
  /// </summary>
  public class FadeOutAnimation : SpriteAnimation
  {
    private TimeSpan fadeOutSpan;

    private float stepsize = 1f;

    public FadeOutAnimation(LazyMonoLib.Interfaces.ISprite sprite, TimeSpan fadeOutSpan)
    {
      this.fadeOutSpan = fadeOutSpan;

      stepsize = (float)sprite.Coloration.A / (float)fadeOutSpan.TotalMilliseconds;
    }

    public override void NextStep(LazyMonoLib.Interfaces.ISprite sprite, GameTime gameTime, ICamera camera)
    {
      if (fadeOutSpan.Ticks > 0)
      {
        sprite.Coloration = new Color(sprite.Coloration, (int)((float)sprite.Coloration.A - stepsize));
        fadeOutSpan = fadeOutSpan - gameTime.ElapsedGameTime;
      }
    }

    public override bool IsFinished
    {
      get
      {
        return fadeOutSpan.Ticks <= 0;
      }
    }
  }
}
