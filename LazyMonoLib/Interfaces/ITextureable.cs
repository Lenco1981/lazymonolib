﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LazyMonoLib.Interfaces
{
  public interface ITextureable
  {
    Texture2D Texture { get; set; }

    Rectangle? SourceRectangle { get; set; }

    Dictionary<string, SubtextureDefinition> Subtextures { get; set; }

    void SetTextureBundle(TextureBundle bundle);

    void SetToSubtextureNamed(string subtextureName);
  }
}
