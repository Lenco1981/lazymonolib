﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Controller;
using Sandbox.Views;

namespace Sandbox.Controller
{
  public class SplashScreenController : BaseController<SplashScreenView>
  {
    public SplashScreenController()
    {
      View = new SplashScreenView(this);

      View.InitializeBackground();
      
      View.InitializeScene();

      View.InitializeUI();

    }

    public override void OnGetActive()
    {
      (View as SplashScreenView).StartAnimation();
    }

    public override void OnGetInactive()
    {
      
    }

    public override bool CanGetInactive()
    {
      return true;
    }

    public void SwitchToMainMenu()
    { }
  }
}
