﻿using LazyMonoLib.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.EventHandler
{
  //public delegate void MouseEventHandler(object sender, MouseEventArgs e);

  public delegate void KeyboardEventHandler(object sender, KeyEventArgs e);
}
