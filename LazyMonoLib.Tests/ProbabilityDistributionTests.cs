﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Tests
{
  public class ProbabilityDistributionTests
  {
    [Test]
    public void ProbabilityDistributionFiftyFityTest()
    {
      ProbabilityDistributionList<string> list = new ProbabilityDistributionList<string>();
      list.Add(0.5m, "a");
      list.Add(0.5m, "b");

      int aCount = 0;
      int bCount = 0;

      for (int i = 0; i < 100000; i++)
      {
        if (list.GetRandomEntry() == "a")
          aCount++;
        else
          bCount++;
      }

      Console.WriteLine(aCount);
      Console.WriteLine(bCount);

      Assert.That(MoreMathHelper.IsInClosedInterval(aCount, 40000, 60000));
      Assert.That(MoreMathHelper.IsInClosedInterval(bCount, 40000, 60000));
    }

    [Test]
    public void ProbabilityDistributionComplexExampleTest()
    {
      ProbabilityDistributionList<string> list = new ProbabilityDistributionList<string>();
      list.Add(0.1m, "a");
      list.Add(0.5m, "b");
      list.Add(0.1m, "c");
      list.Add(0.1m, "d");
      list.Add(0.2m, "e");

      Dictionary<string, int> counter = new Dictionary<string, int> { { "a", 0 }, { "b", 0 }, { "c", 0 }, { "d", 0 }, { "e", 0 } };

      int totalCount = 100000;
      int delta = 500;

      for (int i = 0; i < totalCount; i++)
      {
        string randomEntry = list.GetRandomEntry();
        counter[randomEntry]++;
      }

      foreach (string entry in counter.Keys)
      {
        ProbabilityDistributionEntry<string> propDistEntry = list.FindEntry(entry);

        Console.WriteLine(entry + ": " + counter[entry].ToString());

        int leftBound = (int)((propDistEntry.ProbabilityRangeEnd - propDistEntry.ProbabilityRangeStart) * (decimal)totalCount) - delta;
        int rightBound = (int)((propDistEntry.ProbabilityRangeEnd - propDistEntry.ProbabilityRangeStart) * (decimal)totalCount) + delta;


        Assert.That(MoreMathHelper.IsInClosedInterval(counter[entry], leftBound, rightBound));
      }
    }
  }
}
