﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib
{
  public static class MouseStateExtensions
  {
    public static Point ToPoint(this MouseState mouseState)
    {
      return new Point(mouseState.X, mouseState.Y);
    }

    public static Vector2 ToVector2(this MouseState mouseState)
    {
      return new Vector2(mouseState.X, mouseState.Y);
    }

    public static Vector3 ToVector3(this MouseState mouseState)
    {
      return ToVector3(mouseState, 0f);
    }

    public static Vector3 ToVector3(this MouseState mouseState, float desiredZCoord)
    {
      return new Vector3(mouseState.X, mouseState.Y, desiredZCoord);
    }
  }
}
