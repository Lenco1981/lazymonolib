﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Enums
{
  public enum DialogResult { None, OK, Cancel, Yes, No, Retry, Ignore, Abort }
}
