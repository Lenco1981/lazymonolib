﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Interfaces
{
  public interface IEntity : ILazyUpdateable, ILazyDrawable
  {
    Vector2 CenterPosition { get; set; }
    
    Color Coloration { get; set; }
    
    string Name { get; set; }
    
    Vector2 Position { get; set; }
    
    Vector2 Size { get; set; }
    
    bool DeleteMe { get; set; }
  }
}
