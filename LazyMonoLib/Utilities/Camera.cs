﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Utilities
{
  /// <summary>
  /// A normal non-zoomable camera for transforming local coordinates into world coordinates
  /// </summary>
  public class Camera : ICamera
  {
    private Vector2 position = Vector2.Zero;
    private Vector2 viewPortSize = Vector2.Zero;
    private Rectangle worldRectangle = new Rectangle(0, 0, 0, 0);

    //TODO ZoomFactor!

    /// <summary>
    /// Creates a default camera
    /// </summary>
    public Camera()
    {
      Position = Vector2.Zero;
      ViewPortSize = Global.ScreenSize;
      worldRectangle = new Rectangle((int)position.X, (int)position.Y, Global.ScreenWidth, Global.ScreenHeight);
    }

    /// <summary>
    /// Creates a camera with the given camera position and predefined viewport size
    /// </summary>
    public Camera(Vector2 cameraPosition, Vector2 cameraViewportSize)
    {
      this.position = cameraPosition;
      this.viewPortSize = cameraViewportSize;

      worldRectangle = new Rectangle((int)position.X, (int)position.Y, (int)cameraViewportSize.X, (int)cameraViewportSize.Y);
    }

    /// <summary>
    /// The position of the camera
    /// </summary>
    public Vector2 Position
    {
      get { return position; }
      set
      {
        position = new Vector2(
          MathHelper.Clamp(value.X, worldRectangle.X, worldRectangle.Width - viewPortSize.X),
          MathHelper.Clamp(value.Y, worldRectangle.Y, worldRectangle.Height - viewPortSize.Y));
      }
    }

    /// <summary>
    /// The size of the current camera viewport
    /// </summary>
    public Vector2 ViewPortSize { get { return viewPortSize; } set { viewPortSize = value; } }

    /// <summary>
    /// Transforms a point from the local coordinates to the world coordinates
    /// </summary>
    public Vector2 Transform(Vector2 point)
    {
      return point + Position;
    }

    public Rectangle Transform(Rectangle rectangle)
    {
      return new Rectangle(
        rectangle.Left + (int)Position.X,
        rectangle.Top + (int)Position.Y,
        rectangle.Width,
        rectangle.Height);
    }

    public static ICamera DefaultCamera
    {
      get
      {
        return new Camera { ViewPortSize = Global.ScreenSize, Position = Vector2.Zero };
      }
    }

    public Vector2 CenterPosition
    {
      get { return new Vector2(ViewPortSize.X / 2f, ViewPortSize.Y / 2f); }
    }
  }
}
