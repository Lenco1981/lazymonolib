﻿using LazyMonoLib;
using LazyMonoLib.ContentProvider;
using LazyMonoLib.Enums;
using LazyMonoLib.Modifiers;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox
{
  public class Zombie : Sprite
  {
    public int HitPoints = 40;

    public bool IsMoving { get { return walking != null; } }

    private LinearMovementModifier walking = null;

    private TextureBundle textureBundle;

    public Zombie()
    {
      textureBundle = Global.ContentProvider.GetTextureBundle("rock_enemy_red");
    }

    private Rectangle standing;

    private IModifier walkingEastAnimation;
    private IModifier walkingWestAnimation;
    private IModifier walkingNorthAnimation;
    private IModifier walkingSouthAnimation;

    private IModifier fightingEastAnimation;
    private IModifier fightingWestAnimation;
    private IModifier fightingNorthAnimation;
    private IModifier fightingSouthAnimation;

    private RectangleConstantTimeAnimation dyingAnimation;

    public void Initialize()
    {
      Size = new Vector2(35, 35);

      standing = textureBundle.Subtextures["standing"].Rectangle;

      SetSourceRectangle(standing);

      walkingEastAnimation = new RectangleConstantTimeAnimation(new TimeSpan(0, 0, 0, 0, 100), true,
        textureBundle.Subtextures["walkingEast01"].Rectangle,
        textureBundle.Subtextures["walkingEast02"].Rectangle,
        textureBundle.Subtextures["walkingEast03"].Rectangle,
        textureBundle.Subtextures["walkingEast04"].Rectangle);

      walkingNorthAnimation = new RectangleConstantTimeAnimation(new TimeSpan(0, 0, 0, 0, 100), true,
        textureBundle.Subtextures["walkingNorth01"].Rectangle,
        textureBundle.Subtextures["walkingNorth02"].Rectangle,
        textureBundle.Subtextures["walkingNorth03"].Rectangle,
        textureBundle.Subtextures["walkingNorth04"].Rectangle);

      walkingWestAnimation = new RectangleConstantTimeAnimation(new TimeSpan(0, 0, 0, 0, 100), true,
        textureBundle.Subtextures["walkingWest01"].Rectangle,
        textureBundle.Subtextures["walkingWest02"].Rectangle,
        textureBundle.Subtextures["walkingWest03"].Rectangle,
        textureBundle.Subtextures["walkingWest04"].Rectangle);

      walkingSouthAnimation = new RectangleConstantTimeAnimation(new TimeSpan(0, 0, 0, 0, 100), true,
        textureBundle.Subtextures["walkingSouth01"].Rectangle,
        textureBundle.Subtextures["walkingSouth02"].Rectangle,
        textureBundle.Subtextures["walkingSouth03"].Rectangle,
        textureBundle.Subtextures["walkingSouth04"].Rectangle);

      dyingAnimation = new RectangleConstantTimeAnimation(new TimeSpan(0, 0, 0, 0, 200), true,
        textureBundle.Subtextures["dying01"].Rectangle,
        textureBundle.Subtextures["dying02"].Rectangle,
        textureBundle.Subtextures["dying03"].Rectangle,
        textureBundle.Subtextures["dying04"].Rectangle,
        textureBundle.Subtextures["dying05"].Rectangle,
        textureBundle.Subtextures["dying06"].Rectangle);

      dyingAnimation.CycleAnimation = false;
      
    }

    public void MoveTo(Vector2 destination)
    {
      walking = new LinearMovementModifier(this, this.CenterLocation, destination, LazyRandom.NextFloat());
      AddModifier(walking);
      AddModifier(getWalkingAnimationByRadians(walking.MovementDirectionInRadians));
    }

    private const double PI_180 = Math.PI / 180d;

    private IModifier getWalkingAnimationByRadians(float radians)
    {
      switch (walking.DirectionAsCompassFourth())
      {
        case CompassFourth.East:
          return walkingEastAnimation;
        case CompassFourth.North:
          return walkingNorthAnimation;
        case CompassFourth.West:
          return walkingWestAnimation;
        case CompassFourth.South:
        default:
          return walkingSouthAnimation;
      }
    }

    public bool CanAttack { get { return HitPoints > 0; } }

    public bool DyingComplete { get { return (HitPoints <= 0 && HasModifier(dyingAnimation) && dyingAnimation.IsFinished); } }

    internal void Die()
    {
      AddModifier(dyingAnimation);
    }

    public override void Update(GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState)
    {
      base.Update(gameTime, keyboardState, mouseState, gamePadState);

      if (walking != null && walking.IsFinished)
      {
        ClearModifier();
        walking = null;
      }


      if (HitPoints <= 0)
        letZombieDie();

      if (DyingComplete)
        DeleteMe = true;
    }

    private void letZombieDie()
    {
      ClearModifier();
      AddModifier(dyingAnimation);
    }
  }
}
