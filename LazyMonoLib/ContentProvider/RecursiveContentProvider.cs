﻿using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LazyMonoLib.ContentProvider
{
	public class RecursiveContentProvider : BaseContentProvider, IContentProvider
	{
		public RecursiveContentProvider(ContentManager contentManager)
			: base(contentManager)
		{ }

		public void LoadContent()
		{
			loadLanguages(Path.Combine(ContentRootFolder, LanguagesSubfolder));
			loadFonts(Path.Combine(ContentRootFolder, FontsSubfolder));
			loadTextures(Path.Combine(ContentRootFolder, TexturesSubfolder));
		}

		private void loadLanguages(string languagesRootPath)
		{

			foreach (string file in Directory.GetFiles(languagesRootPath, "*.data"))
			{
				loadLanguageFromFile(file);
			}
		}

		private void loadFonts(string fontRootPath)
		{
			foreach (string file in Directory.GetFiles(fontRootPath, "*.xnb"))
			{
				loadFontFromFile(file);
			}

			foreach (string subfolder in Directory.GetDirectories(fontRootPath))
			{
				loadFonts(subfolder);
			}
		}

		public void loadTextures(string texturesRootPath)
		{
			foreach (string file in Directory.GetFiles(texturesRootPath, "*.xnb"))
			{
				loadTextureFromFile(file);
			}

			foreach (string subfolder in Directory.GetDirectories(texturesRootPath))
			{
				loadTextures(subfolder);
			}
		}
	}
}
