﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Xna.Framework
{
  public static class VectorExtensions
  {

    public static Point ToPoint(this Vector2 vector)
    {
      return new Point((int)vector.X, (int)vector.Y);
    }
  }
}
