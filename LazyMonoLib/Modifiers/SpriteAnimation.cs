﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  public abstract class SpriteAnimation : IModifier
  {
    public abstract void NextStep(ISprite sprite, GameTime gameTime, ICamera camera);

    public abstract bool IsFinished { get; }
  }
}
