﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Controller
{
  public static class ControllerList
  {
    static ControllerList()
    {
      TestController = new TestController();

      //MainController = new MainController();

      //SplashScreenController = new SplashScreenController();

      //HauptmenüController = new HauptmenüController();
    }

    public static TestController TestController { get; private set; }

    //public static MainController MainController { get; private set; }

    //public static SplashScreenController SplashScreenController { get; private set; }

    //public static HauptmenüController HauptmenüController { get; private set; }
  }
}
