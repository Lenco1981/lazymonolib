﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LazyMonoLib.Serializer
{
  public abstract class XmlProbabilityEntry : XmlEnumEntry
  {
    public float Probability { get; set; }

    public override string ToString()
    {
      return String.Format("{0} [{1}]", base.ToString(), Probability.ToString("P"));
    }
  }
}
