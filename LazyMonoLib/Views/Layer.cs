﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;

namespace LazyMonoLib.Views
{
	public class Layer : ILazyDrawable, ILazyUpdateable
	{
		public List<IEntity> Entities { get; set; }

		public int Index { get; set; }

		public Layer(int index)
		{
			Index = index;
			Entities = new List<IEntity>();
			DoUpdate = true;
			IsVisible = true;
		}

		

		public void Update(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState, Utilities.ICamera camera)
		{
			foreach (IEntity entity in Entities)
			{
				if (entity.DoUpdate)
					entity.Update(gameTime, keyboardState, mouseState, gamePadState, camera);
			}
		}

		public bool DoUpdate { get; set; }

		public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch, Utilities.ICamera camera)
		{
			foreach (IEntity entity in Entities)
			{
				if (entity.IsVisible)
					entity.Draw(gameTime, spriteBatch, camera);
			}
		}

		public bool IsVisible { get; set; }

		public void Add(IEntity entity)
		{
			Entities.Add(entity);
		}

		public void Remove(IEntity entity)
		{
			Entities.Remove(entity);
		}

		public bool Contains(IEntity entity)
		{
			return Entities.Contains(entity);
		}
	}
}
