﻿using LazyMonoLib.Controller;
using LazyMonoLib.Controls;
using LazyMonoLib.EventArgs;
using LazyMonoLib.Interfaces;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LazyMonoLib.Views
{
	/// <summary>
	/// Interface for representing a drawable and updateable basic view instance.
	/// With three different layers (background, scene and UI).
	/// </summary>
	public interface IView
	{
		void InitializeLayers();

		Layers Layers { get; set; }

		ICamera Camera { get; set; }

		/// <summary>
		/// Primary update routine for each view. Updates Background, Scene and UI collection of entities.
		/// </summary>
		void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState);

		/// <summary>
		/// Primary draw routine for each view. Draws Background, Scene and UI collection of entities.
		/// </summary>
		void Draw(GameTime gameTime, SpriteBatch spriteBatch);
	}
}
