﻿using LazyMonoLib.Controller;
using Sandbox.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Controller
{
  public class TestController : BaseController<TestView>
  {
    public TestController()
    {
      View = new TestView(this);

      View.InitializeBackground();

      View.InitializeScene();

      View.InitializeUI();
    }

    public override void OnGetActive()
    {
      
    }

    public override void OnGetInactive()
    {
      
    }

    public override bool CanGetInactive()
    {
      return true;
    }
  }
}
