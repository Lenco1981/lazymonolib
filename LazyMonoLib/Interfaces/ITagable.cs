﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Interfaces
{
  public interface ITagable
  {
    object Tag { get; set; }

    K TagAs<K>();

    bool HasTag();
  }
}
