﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Interfaces
{
  public interface ISprite : IEntity, Interfaces.IModifiable, Interfaces.ITextureable
  {
  }
}
