﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Animates the source rectangle of a sprite over time. Each rectangle is shown for a separate given amount of time
  /// </summary>
  public class RectangleDifferentTimeAnimation : SpriteAnimation
  {
    public List<RectangleTimeSpan> Rectangles = new List<RectangleTimeSpan>();

    public int CurrentIndex = 0;

    public RectangleTimeSpan CurrentRectangleTimeSpan { get { return Rectangles[CurrentIndex]; } }

    private TimeSpan lastChange;

    private bool started = false;

    public override void NextStep(ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      if (!started)
      {
        started = true;
        lastChange = Rectangles[0].DisplayTimeSpan;
      }
      else
      {
        lastChange = lastChange - gameTime.ElapsedGameTime;

        if (isNextFrameNeeded(gameTime))
        {
          CurrentIndex++;

          lastChange = Rectangles[CurrentIndex].DisplayTimeSpan;

          if (CurrentIndex == Rectangles.Count)
            CurrentIndex = 0;              
        }
      }

      sprite.SourceRectangle = Rectangles[CurrentIndex].Rectangle;
    }

    private bool isNextFrameNeeded(GameTime gameTime)
    {
      return lastChange.Ticks > 0;
    }

    public override bool IsFinished
    {
      get { return false; }
    }
  }
}
