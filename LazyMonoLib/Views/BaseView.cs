﻿using LazyMonoLib.Controller;
using LazyMonoLib.Controls;
using LazyMonoLib.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.ContentProvider;

namespace LazyMonoLib.Views
{
  /// <summary>
  /// An abstract base view containing a reference to a strong typed controller via generics
  /// </summary>
  /// <typeparam name="CONTROLLER_TYPE"></typeparam>
  public abstract class BaseView<CONTROLLER_TYPE> : AbstractView, IView
    where CONTROLLER_TYPE : IController
  {
    protected CONTROLLER_TYPE controller { get; private set; }

    protected IContentProvider contentProvider { get { return Global.ContentProvider; } }

    public BaseView(CONTROLLER_TYPE controller)
      : base()
    {
      this.controller = controller;
    }
  }
}
