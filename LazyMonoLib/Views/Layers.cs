﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;

namespace LazyMonoLib.Views
{
	public class Layers : ILazyUpdateable, ILazyDrawable
	{
		private SortedList<int, Layer> layers { get; set; }

		public Layers()
		{
			layers = new SortedList<int, Layer>();
			DoUpdate = true;
			IsVisible = true;
		}

		public void AddLayer(Layer layer)
		{
			layers.Add(layer.Index, layer);
		}

		public void AddLayers(params Layer[] paramLayers)
		{
			foreach (Layer layer in paramLayers)
				AddLayer(layer);
		}

		public void RemoveLayer(Layer layer)
		{
			layers.Remove(layer.Index);
		}

		public Layer this[int key]
		{
			get
			{
				return layers[key];
			}
			set
			{
				layers[key] = value;
			}
		}

		public void Update(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState, Utilities.ICamera camera)
		{
			if (DoUpdate)
			{
				foreach (Layer layer in layers.Values)
				{
					if (layer.DoUpdate)
						layer.Update(gameTime, keyboardState, mouseState, gamePadState, camera);
				}
			}
		}

		public bool DoUpdate { get; set; }

		public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch, Utilities.ICamera camera)
		{
			foreach (Layer layer in layers.Values)
			{
				if (layer.IsVisible)
					layer.Draw(gameTime, spriteBatch, camera);
			}
		}

		public bool IsVisible { get; set; }
	}
}
