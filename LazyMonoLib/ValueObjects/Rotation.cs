﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.ValueObjects
{
  public class Rotation
  {
    private const float deg2Rad = ((float)Math.PI / 180f);
    private const float rad2Deg = (180f / (float)Math.PI);

    public float Radians { get; set; }

    public Rotation(float presetRadians)
    {
      Radians = presetRadians;
    }

    public Rotation()
    {
      Radians = 0;
    }

    public void AddDegrees(float degrees)
    {
      Radians += (degrees * deg2Rad);
    }

    public float Degrees { get { return Radians * rad2Deg; } set { Radians = deg2Rad * value; } }

    public static Rotation _90Degrees = new Rotation((float)Math.PI * 0.5f);

    public static Rotation _180Degrees = new Rotation((float)Math.PI);

    public static Rotation _270Degrees = new Rotation((float)Math.PI * 1.5f);

    public static Rotation _360Degrees = new Rotation((float)Math.PI * 2f);
  }
}
