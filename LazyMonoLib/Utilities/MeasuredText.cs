﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LazyMonoLib.Utilities
{
  public class MeasuredText
  {
    private String text = String.Empty;

    private SpriteFont font = null;

    private float? maxWidth = null;
    
    private Vector2 size = Vector2.Zero;

    public MeasuredText(SpriteFont font, string text)
    {
      this.font = font;
      this.text = text;

      refreshMeasure();
    }

    public Vector2 Size { get { return size; } }

    public SpriteFont Font { get { return font; } set { font = value; refreshMeasure(); } }

    public String Text { get { return text; } set { text = value; refreshMeasure(); } }

    public float? MaxWidth { get { return maxWidth; } set { maxWidth = value; refreshMeasure(); } }

    private void refreshMeasure()
    {
      if (maxWidth.HasValue)
      {
        text = textToMultilineText(text);
      }

      size = font.MeasureString(text);
    }

    private string textToMultilineText(string text)
    {
      StringBuilder displayText = new StringBuilder();
      StringBuilder preview = new StringBuilder();

      Vector2 currentTextSize = Vector2.Zero;

      foreach (string word in text.SplitIntoList(" "))
      {
        preview.AppendFormat("{0} ", word);
        currentTextSize = Font.MeasureString(preview);

        if (currentTextSize.X > maxWidth.Value)
        {
          displayText.AppendLine();
          displayText.AppendFormat("{0} ", word);
          preview.Clear();
        }
        else
        {
          displayText.AppendFormat("{0} ", word);
        }
      }
      return displayText.ToString();
    }
  }
}
