﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Modifiers;
using LazyMonoLib.Utilities;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib
{
  /// <summary>
  /// Base class for a textured sprite. Contains all drawing and update logic
  /// </summary>
  public class Sprite : Entity, LazyMonoLib.Interfaces.ISprite
  {
    /// <summary>
    /// List of modifier to change properties or add animations
    /// </summary>
    protected List<IModifier> modifierList = new List<IModifier>();

    public Texture2D Texture { get; set; }

    public Dictionary<string, SubtextureDefinition> Subtextures { get; set; }

    public Sprite(string name)
    {
      Name = name;
    }

    public Sprite()
    { }

    public void AddModifier(IModifier modifier)
    {
      modifierList.Add(modifier);
    }

    public void RemoveModifier(IModifier modifier)
    {
      modifierList.Remove(modifier);
    }

    public void ClearModifier()
    {
      modifierList.Clear();
    }

    public bool HasModifier(IModifier modifier)
    {
      return modifierList.Contains(modifier);
    }

    public bool HasModifierOfType<K>()
    {
      return modifierList.Any(x => x is K);
    }

    public K GetFirstModifierOfType<K>()
    {
      return (K)modifierList.First(x => x is K);
    }

    public List<K> GetModifierOfType<K>()
    {
      return modifierList.FindAll(x => x is K).ConvertAll<K>(x => (K)x);
    }

    public int ModifierCount
    {
      get { return modifierList.Count; }
    }

    public void SetTextureBundle(TextureBundle bundle)
    {
      Texture = bundle.Texture2D;

      Subtextures = bundle.Subtextures;
    }

    public void SetToSubtextureNamed(string subtextureName)
    {
      SourceRectangle = Subtextures[subtextureName].Rectangle;
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      if (!IsVisible)
        return;

      spriteBatch.Draw(Texture, getDestinationRectangle(camera), sourceRectangle, coloration);
    }

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera)
    {
      processModifiers(gameTime, camera);
    }

    protected void processModifiers(GameTime gameTime, ICamera camera)
    {
      for (int i = 0; i < modifierList.Count; i++)
      {
        if (modifierList[i] == null)
          continue;

        modifierList[i].NextStep(this, gameTime, camera);

        if (modifierList[i].IsFinished)
           modifierList.RemoveAt(i);
 

      }
    }
  }
}
