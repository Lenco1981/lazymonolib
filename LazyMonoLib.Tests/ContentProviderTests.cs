﻿using LazyMonoLib.ContentProvider;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Tests
{
  public class ContentProviderTests
  {
    public ContentProviderTests()
      : base()
    { }

    private RecursiveContentProvider provider = new RecursiveContentProvider(null);

    [Test]
    public void ConvertFullFilePathToContentFileTest()
    {
      
    



      //Texture2D texture = Content.Load<Texture2D>(provider.convertFilepathToContentTarget(@"Content\Textures\person.xnb"));

      //Assert.That(texture, Is.Not.Null);
    }

    [Test]
    public void GetTextureNameFromFilenameTest()
    {
      string name = provider.getTextureNameFromFilename(@"Content\Textures\person.xnb");

      Assert.That(name, Is.EqualTo("person"));
    }

  }
}
