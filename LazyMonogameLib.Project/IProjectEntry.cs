﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyMonogameLib.Project
{
	public interface IProjectEntry
	{
		string DisplayName { get; set; }

		string FullPath { get; set; }

		Uri Icon { get; set; }

		List<IProjectEntry> Childs { get; set; }

		bool HasChilds { get; }
	}
}
