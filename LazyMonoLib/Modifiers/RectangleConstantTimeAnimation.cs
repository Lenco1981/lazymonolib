﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Changes the source rectangles of a sprite over time. Showing each rectangle for the same amount of given time.
  /// </summary>
  public class RectangleConstantTimeAnimation : SpriteAnimation
  {
    public List<Rectangle> Rectangles = new List<Rectangle>();

    public TimeSpan TimeSpanPerRectangle = new TimeSpan(0, 0, 1);
    
    public int CurrentIndex = 0;

    public Rectangle CurrentRectangle { get { return Rectangles[CurrentIndex]; } }

    private TimeSpan lastChange = TimeSpan.Zero;

    public bool CycleAnimation = false;

    public RectangleConstantTimeAnimation()
    { }

    public RectangleConstantTimeAnimation(TimeSpan timeSpanPerRectangle, params Rectangle[] rectangles)
      : this(timeSpanPerRectangle, false, rectangles)
    {

    }

    public RectangleConstantTimeAnimation(TimeSpan timeSpanPerRectangle, bool cycleAnimation, params Rectangle[] rectangles)
    {
      Rectangles = rectangles.ToList();
      TimeSpanPerRectangle = timeSpanPerRectangle;
      lastChange = timeSpanPerRectangle;
      CycleAnimation = cycleAnimation;
    }

    public override void NextStep(ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      lastChange = lastChange - gameTime.ElapsedGameTime;

      if (isNextFrameNeeded())
      {
        CurrentIndex++;

        if (CurrentIndex == Rectangles.Count - 1 && CycleAnimation)
          CurrentIndex = 0;
        
        


        lastChange = TimeSpanPerRectangle;

        if (CurrentIndex < Rectangles.Count)
          sprite.SourceRectangle = Rectangles[CurrentIndex];
      }
    }

    private bool isNextFrameNeeded()
    {
      return lastChange.Ticks <= 0;
    }

    public override bool IsFinished
    {
      get { return !CycleAnimation && CurrentIndex == Rectangles.Count - 1; }
    }

  }
}
