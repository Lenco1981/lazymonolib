﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib
{
  /// <summary>
  /// Initialize an instance of the random class. Contains some extra methods for get random
  /// numbers.
  /// </summary>
  public static class LazyRandom
  {
    private static System.Random r = new System.Random();

    /// <summary>
    /// Returns a random float.
    /// </summary>
    public static float NextFloat()
    {
      return (float)r.NextDouble();
    }

    /// <summary>
    /// Returns a random float with a given maximum
    /// </summary>
    public static float NextFloat(float max)
    {
      return ((float)r.NextDouble() * max);
    }

    public static decimal NextDecimal()
    {
      return (decimal)r.NextDouble();
    }

    /// <summary>
    /// Returns a random integer with a given maximum
    /// </summary>
    public static int NextInt(int max)
    {
      return r.Next(max);
    }
  }
}
