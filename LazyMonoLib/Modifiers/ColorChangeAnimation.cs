﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Enums;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Changes between to given colors by iterating all color channels separately over the given time
  /// </summary>
  public class ColorChangeAnimation : IModifier
  {
    private Color startColor;

    private Color endColor;

    private double stepR, stepG, stepB, stepA;

    private double currentR, currentG, currentB, currentA;

    private TimeSpan duration;

    private ColorPartsAffected partsAffected = ColorPartsAffected.All;

    /// <summary>
    /// Gets the current color during the color change animation
    /// </summary>
    public Color CurrentColor { get; private set; }

    /// <summary>
    /// Creates a new ColorChangeAnimation with a given duration, start and end color affecting all 
    /// parts of the color struct during the transformation.
    /// </summary>
    public ColorChangeAnimation(Color startColor, Color endColor, TimeSpan duration)
      : this(startColor, endColor, duration, ColorPartsAffected.All)
    { }

    /// <summary>
    /// Create a new ColorChangeAnimation with a given duration, start and end color. Affecting only
    /// the given color part
    /// </summary>
    public ColorChangeAnimation(Color startColor, Color endColor, TimeSpan duration, ColorPartsAffected colorPartsAffected)
    {
      this.startColor = startColor;
      this.endColor = endColor;
      this.duration = duration;
      this.partsAffected = colorPartsAffected;

      initializeAnimation();
      
    }

    private void initializeAnimation()
    {
      currentR = startColor.R;
      currentG = startColor.G;
      currentB = startColor.B;
      currentA = startColor.A;


      stepR = ((double)endColor.R - (double)startColor.R) / duration.TotalMilliseconds;
      stepG = ((double)endColor.G - (double)startColor.G) / duration.TotalMilliseconds;
      stepB = ((double)endColor.B - (double)startColor.B) / duration.TotalMilliseconds;
      stepA = ((double)endColor.A - (double)startColor.A) / duration.TotalMilliseconds;

    }

    /// <summary>
    /// Iterates the next step in the color change modifier.
    /// </summary>
    public void NextStep(ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      //duration = duration - gameTime.ElapsedGameTime;

      if (CurrentColor.R != endColor.R)
        currentR = currentR + (stepR * gameTime.ElapsedGameTime.TotalMilliseconds);

      if (CurrentColor.G != endColor.G)
        currentG = currentG + (stepG * gameTime.ElapsedGameTime.TotalMilliseconds);

      if (CurrentColor.B != endColor.B)
        currentB = currentB + (stepB * gameTime.ElapsedGameTime.TotalMilliseconds);

      if (CurrentColor.A != endColor.A)
        currentA = currentA + (stepA * gameTime.ElapsedGameTime.TotalMilliseconds);

      CurrentColor = new Color((int)currentR, (int)currentG, (int)currentB, (int)currentA);

      sprite.Coloration = CurrentColor;
    }

    /// <summary>
    /// Is 'true' if the color change transformation is finished and the modifier can be deleted.
    /// </summary>
    public bool IsFinished
    {
      get { return CurrentColor == endColor; }
    }
  }
}
