﻿namespace LazyMonogameEditorWinForms
{
	partial class UcSpriteSheetEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pictureBoxSpriteSheet = new System.Windows.Forms.PictureBox();
			this.panelSpriteSheet = new System.Windows.Forms.Panel();
			this.txtTexturePath = new System.Windows.Forms.TextBox();
			this.btnLoad = new System.Windows.Forms.Button();
			this.treeView = new System.Windows.Forms.TreeView();
			this.colorDialog1 = new System.Windows.Forms.ColorDialog();
			this.button1 = new System.Windows.Forms.Button();
			this.panelBackgroundColor = new System.Windows.Forms.Panel();
			this.trackBarZoom = new System.Windows.Forms.TrackBar();
			this.labelZoom = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpriteSheet)).BeginInit();
			this.panelSpriteSheet.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarZoom)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBoxSpriteSheet
			// 
			this.pictureBoxSpriteSheet.Location = new System.Drawing.Point(-1, -1);
			this.pictureBoxSpriteSheet.Name = "pictureBoxSpriteSheet";
			this.pictureBoxSpriteSheet.Size = new System.Drawing.Size(800, 600);
			this.pictureBoxSpriteSheet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxSpriteSheet.TabIndex = 0;
			this.pictureBoxSpriteSheet.TabStop = false;
			// 
			// panelSpriteSheet
			// 
			this.panelSpriteSheet.AutoScroll = true;
			this.panelSpriteSheet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelSpriteSheet.Controls.Add(this.pictureBoxSpriteSheet);
			this.panelSpriteSheet.Location = new System.Drawing.Point(3, 77);
			this.panelSpriteSheet.Name = "panelSpriteSheet";
			this.panelSpriteSheet.Size = new System.Drawing.Size(800, 600);
			this.panelSpriteSheet.TabIndex = 1;
			// 
			// txtTexturePath
			// 
			this.txtTexturePath.Location = new System.Drawing.Point(3, 29);
			this.txtTexturePath.Name = "txtTexturePath";
			this.txtTexturePath.Size = new System.Drawing.Size(694, 20);
			this.txtTexturePath.TabIndex = 2;
			this.txtTexturePath.Text = "D:\\Entwicklung\\ZombieSurvival\\XnaDummyContent\\Textures\\Persons\\human_player_yello" +
    "w.png";
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(728, 26);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(75, 23);
			this.btnLoad.TabIndex = 3;
			this.btnLoad.Text = "Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// treeView
			// 
			this.treeView.Location = new System.Drawing.Point(830, 77);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(402, 600);
			this.treeView.TabIndex = 4;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(453, 684);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(122, 23);
			this.button1.TabIndex = 5;
			this.button1.Text = "Background Color";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panelBackgroundColor
			// 
			this.panelBackgroundColor.BackColor = System.Drawing.Color.Black;
			this.panelBackgroundColor.Location = new System.Drawing.Point(581, 684);
			this.panelBackgroundColor.Name = "panelBackgroundColor";
			this.panelBackgroundColor.Size = new System.Drawing.Size(52, 23);
			this.panelBackgroundColor.TabIndex = 6;
			// 
			// trackBarZoom
			// 
			this.trackBarZoom.Location = new System.Drawing.Point(133, 683);
			this.trackBarZoom.Maximum = 400;
			this.trackBarZoom.Minimum = 1;
			this.trackBarZoom.Name = "trackBarZoom";
			this.trackBarZoom.Size = new System.Drawing.Size(183, 45);
			this.trackBarZoom.TabIndex = 7;
			this.trackBarZoom.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarZoom.Value = 100;
			this.trackBarZoom.ValueChanged += new System.EventHandler(this.trackBarZoom_ValueChanged);
			// 
			// labelZoom
			// 
			this.labelZoom.AutoSize = true;
			this.labelZoom.Location = new System.Drawing.Point(82, 689);
			this.labelZoom.Name = "labelZoom";
			this.labelZoom.Size = new System.Drawing.Size(34, 13);
			this.labelZoom.TabIndex = 8;
			this.labelZoom.Text = "Zoom";
			// 
			// UcSpriteSheetEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.labelZoom);
			this.Controls.Add(this.trackBarZoom);
			this.Controls.Add(this.panelBackgroundColor);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.treeView);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.txtTexturePath);
			this.Controls.Add(this.panelSpriteSheet);
			this.Name = "UcSpriteSheetEditor";
			this.Size = new System.Drawing.Size(1266, 714);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpriteSheet)).EndInit();
			this.panelSpriteSheet.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackBarZoom)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBoxSpriteSheet;
		private System.Windows.Forms.Panel panelSpriteSheet;
		private System.Windows.Forms.TextBox txtTexturePath;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panelBackgroundColor;
		private System.Windows.Forms.TrackBar trackBarZoom;
		private System.Windows.Forms.Label labelZoom;
	}
}
