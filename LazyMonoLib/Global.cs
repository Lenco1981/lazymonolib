﻿using LazyMonoLib.ContentProvider;
using LazyMonoLib.Controller;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LazyMonoLib
{
	/// <summary>
	/// A class containing global objects such as the GraphicsDevice, the ContentManager and -provider.
	/// </summary>
	public static class Global
	{
		private static LazyMonoGame currentGame = null;

		internal static void SetCurrentGame(LazyMonoGame game)
		{
			currentGame = game;
		}

		public static LazyMonoGame Game { get { return currentGame; } }

		public static GraphicsDevice GraphicsDevice { get { return currentGame.GraphicsDevice; } }

		public static IContentProvider ContentProvider { get { return currentGame.ContentProvider; } }

		public static ContentManager ContentManager { get { return currentGame.Content; } }

		public static IController CurrentController { get { return currentGame.CurrentController; } }

		public static int ScreenWidth = 1280;

		public static int ScreenHeight = 720;

		public static Vector2 ScreenCenter { get { return new Vector2(ScreenWidth / 2, ScreenHeight / 2); } }

		public static Vector2 ScreenSize { get { return new Vector2(ScreenWidth, ScreenHeight); } }

		public static void LoadLocalizationFile(string filename)
		{

		}

		private static Dictionary<string, string> localeTexts = new Dictionary<string, string>();

		private static string noLocaleFoundHint = "??";
		//private static string noLocaleFoundHint = "■";

		internal static string Localize(string textToLocalize, params object[] parameters)
		{
			if (localeTexts.ContainsKey(textToLocalize))
			{
				return String.Format(localeTexts[textToLocalize], parameters);
			}
			else
			{
				return String.Format(noLocaleFoundHint + textToLocalize, parameters);
			}
		}
	}
}
