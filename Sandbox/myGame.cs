﻿using LazyMonoLib;
using LazyMonoLib.ContentProvider;
using Sandbox.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox
{
  public class myGame : LazyMonoLib.LazyMonoGame
  {
    public myGame()
    {
      ContentProvider = new RecursiveContentProvider(Content);

      ContentProvider.LoadContent();

      //CurrentController = ControllerList.SplashScreenController;

      //CurrentController = ControllerList.HauptmenüController;
      
      CurrentController = ControllerList.TestController;

      CurrentController.OnGetActive();
    }

    protected override void LoadContent()
    {
      base.LoadContent();
    }
  }
}
