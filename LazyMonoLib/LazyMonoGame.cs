﻿using LazyMonoLib.Controller;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.ContentProvider;

namespace LazyMonoLib
{
  /// <summary>
  /// Abstract class for a lazy mono game. 
  /// </summary>
  public abstract class LazyMonoGame : Game
  {
    protected GraphicsDeviceManager graphicsDeviceManager;

    public IContentProvider ContentProvider { get; set; }

    protected SpriteBatch spriteBatch;

    public IController CurrentController { get; set; }

    protected virtual Color clearColor { get { return Color.CornflowerBlue; } }

    protected GamePadState gamePadState { get { return GamePad.GetState(PlayerIndex.One); } }

    protected MouseState mouseState { get { return Mouse.GetState(); } }
    
    protected KeyboardState keyboardState { get { return Keyboard.GetState(); } }

    public LazyMonoGame()
      : base()
    {
      graphicsDeviceManager = new GraphicsDeviceManager(this);
      graphicsDeviceManager.CreateDevice();

      Content.RootDirectory = "Content";

      Global.SetCurrentGame(this);
    }

    /// <summary>
    /// Sets the current controller. First checks if currentController.CanGetInactive then calling currentController.OnGetInactive() and after that
    /// the newController is set and newController.OnGetActive() is called.
    /// </summary>
    public bool SetCurrentController(IController newController)
    {
      if (CurrentController.CanGetInactive())
      {
        CurrentController.OnGetInactive();

        CurrentController = newController;

        CurrentController.OnGetActive();

        return true;
      }
      else
        return false;
    }

    protected override void Initialize()
    {
      base.Initialize();

      //WICHTIG! WENN DAS NICHT IN INITIALIZE IST, FUNKTIONIERT ES NICHT!
      graphicsDeviceManager.PreferredBackBufferHeight = Global.ScreenHeight;
      graphicsDeviceManager.PreferredBackBufferWidth = Global.ScreenWidth;

      IsMouseVisible = true;

      graphicsDeviceManager.ApplyChanges();
    }  

    protected override void LoadContent()
    {
      spriteBatch = new SpriteBatch(GraphicsDevice);
    }

    protected override void UnloadContent()
    {
     
    }

    /// <summary>
    /// Updates the current view
    /// </summary>
    protected override void Update(GameTime gameTime)
    {
      if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
        Exit();

      CurrentController.View.Update(gameTime, keyboardState, mouseState, gamePadState);

    }

    /// <summary>
    /// Draws the current view
    /// </summary>
    protected override void Draw(GameTime gameTime)
    {
      GraphicsDevice.Clear(clearColor);

      spriteBatch.Begin();

      CurrentController.View.Draw(gameTime, spriteBatch);

      spriteBatch.End();

      base.Draw(gameTime);
    }
  }
}
