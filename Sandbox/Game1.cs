﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using LazyMonoLib;
using LazyMonoLib.Modifiers;
using LazyMonoLib.ValueObjects;
#endregion

namespace Sandbox
{
  /// <summary>
  /// This is the main type for your game
  /// </summary>
  public class Game1 : Game
  {
    GraphicsDeviceManager graphics;
    SpriteBatch spriteBatch;

    private Sprite person;
    private Texture2D personTexture;

    private bool pause = false;

    private RectangleConstantTimeAnimation walkingSouthAnimation = new RectangleConstantTimeAnimation();

    private RectangleConstantTimeAnimation walkingEastAnimation = new RectangleConstantTimeAnimation();

    private RectangleConstantTimeAnimation walkingNorthAnimation = new RectangleConstantTimeAnimation();

    private RectangleConstantTimeAnimation walkingWestAnimation = new RectangleConstantTimeAnimation();

    private Rectangle personStanding = new Rectangle(0, 0, 35, 35);

    public Game1()
      : base()
    {
      graphics = new GraphicsDeviceManager(this);
      
      Content.RootDirectory = "Content";
    }

    /// <summary>
    /// Allows the game to perform any initialization it needs to before starting to run.
    /// This is where it can query for any required services and load any non-graphic
    /// related content.  Calling base.Initialize will enumerate through any components
    /// and initialize them as well.
    /// </summary>
    protected override void Initialize()
    {
      // TODO: Add your initialization logic here

      base.Initialize();
    }

    /// <summary>
    /// LoadContent will be called once per game and is the place to load
    /// all of your content.
    /// </summary>
    protected override void LoadContent()
    {
      // Create a new SpriteBatch, which can be used to draw textures.
      spriteBatch = new SpriteBatch(GraphicsDevice);

      texture = Content.Load<Texture2D>("person");

      personTexture = Content.Load<Texture2D>("human_player_yellow");

      person = new Sprite(); //personTexture);

      person.CenterLocation = new Vector2(400, 400);
      person.Size = new Vector2(35, 35);
      person.Coloration = Color.White;
      person.SetSourceRectangle(new Rectangle(4, 214, 25, 25));

      walkingSouthAnimation.Rectangles.Add(new Rectangle(0, 210, 35, 35));
      walkingSouthAnimation.Rectangles.Add(new Rectangle(35, 210, 35, 35));
      walkingSouthAnimation.Rectangles.Add(new Rectangle(70, 210, 35, 35));
      walkingSouthAnimation.Rectangles.Add(new Rectangle(105, 210, 35, 35));
      walkingSouthAnimation.CycleAnimation = true;
      walkingSouthAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      walkingEastAnimation.Rectangles.Add(new Rectangle(140, 210, 35, 35));
      walkingEastAnimation.Rectangles.Add(new Rectangle(175, 210, 35, 35));
      walkingEastAnimation.Rectangles.Add(new Rectangle(210, 210, 35, 35));
      walkingEastAnimation.Rectangles.Add(new Rectangle(245, 210, 35, 35));
      walkingEastAnimation.CycleAnimation = true;
      walkingEastAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      walkingNorthAnimation.Rectangles.Add(new Rectangle(280, 210, 35, 35));
      walkingNorthAnimation.Rectangles.Add(new Rectangle(315, 210, 35, 35));
      walkingNorthAnimation.Rectangles.Add(new Rectangle(350, 210, 35, 35));
      walkingNorthAnimation.Rectangles.Add(new Rectangle(385, 210, 35, 35));
      walkingNorthAnimation.CycleAnimation = true;
      walkingNorthAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      walkingWestAnimation.Rectangles.Add(new Rectangle(420, 210, 35, 35));
      walkingWestAnimation.Rectangles.Add(new Rectangle(455, 210, 35, 35));
      walkingWestAnimation.Rectangles.Add(new Rectangle(490, 210, 35, 35));
      walkingWestAnimation.Rectangles.Add(new Rectangle(525, 210, 35, 35));
      walkingWestAnimation.CycleAnimation = true;
      walkingWestAnimation.TimeSpanPerRectangle = new TimeSpan(0, 0, 0, 0, 100);

      sprite = new Sprite(); //texture);

      sprite.CenterLocation = new Vector2(200, 200);
      sprite.Size = new Vector2(19, 14);
      sprite.Rectangles.Add(new Rectangle(1, 3, 19, 14));
      sprite.Rectangles.Add(new Rectangle(22, 3, 19, 14));
      sprite.SetSourceRectangle(sprite.Rectangles[0]);
      sprite.Coloration = new Color(255, 255, 255, 255);

      //sprite.AddAnimation(new LazyMonoLib.Animations.FadeOutAnimation(sprite, new TimeSpan(0, 0, 10)));

      // TODO: use this.Content to load your game content here
    }

    protected Sprite sprite = null;
    protected Texture2D texture = null;

    

    /// <summary>
    /// UnloadContent will be called once per game and is the place to unload
    /// all content.
    /// </summary>
    protected override void UnloadContent()
    {
      // TODO: Unload any non ContentManager content here
    }

    private TimeSpan keypress;

    private int delta = 2;

    /// <summary>
    /// Allows the game to run logic such as updating the world,
    /// checking for collisions, gathering input, and playing audio.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    protected override void Update(GameTime gameTime)
    {
      if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
        Exit();

      KeyboardState state = Keyboard.GetState();

      Point mousePoint = new Point(Mouse.GetState().X, Mouse.GetState().Y);

      if (sprite.IsHitBy(mousePoint))      
        sprite.SetSourceRectangle(sprite.Rectangles[0]);
      else
        sprite.SetSourceRectangle(sprite.Rectangles[1]);

      //sprite.Update(gameTime);

      if (state.IsKeyDown(Keys.Down))
      {
        if (person.ModifierCount == 0)
          person.AddModifier(walkingSouthAnimation);

        person.CenterLocation = new Vector2(person.CenterLocation.X, person.CenterLocation.Y + delta);
      }
      else person.RemoveModifier(walkingSouthAnimation);

      if (state.IsKeyDown(Keys.Up))
      {
        if (person.ModifierCount == 0)
          person.AddModifier(walkingNorthAnimation);

        person.CenterLocation = new Vector2(person.CenterLocation.X, person.CenterLocation.Y - delta);
      }
      else person.RemoveModifier(walkingNorthAnimation);
      
      if (state.IsKeyDown(Keys.Right))
      {
        if (person.ModifierCount == 0)
          person.AddModifier(walkingEastAnimation);

        person.CenterLocation = new Vector2(person.CenterLocation.X+delta, person.CenterLocation.Y);
      }
      else person.RemoveModifier(walkingEastAnimation);

      if (state.IsKeyDown(Keys.Left))
      {
        if (person.ModifierCount == 0)
          person.AddModifier(walkingWestAnimation);

        person.CenterLocation = new Vector2(person.CenterLocation.X - delta, person.CenterLocation.Y);
      }
      else
        person.RemoveModifier(walkingWestAnimation);


      if (person.ModifierCount == 0)
        person.SetSourceRectangle(personStanding);

     // person.Update(gameTime);

      // TODO: Add your update logic here

      base.Update(gameTime);
    }



    /// <summary>
    /// This is called when the game should draw itself.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    protected override void Draw(GameTime gameTime)
    {
      GraphicsDevice.Clear(Color.CornflowerBlue);

      spriteBatch.Begin();

      sprite.Draw(gameTime,spriteBatch);

      person.Draw(gameTime,spriteBatch);

      spriteBatch.End();

      // TODO: Add your drawing code here

      base.Draw(gameTime);
    }
  }
}
