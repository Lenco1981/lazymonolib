﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Contains a list of modifier which will be executed at the same time
  /// </summary>
  public class ParallelModifierList : IModifier
  {
    public List<IModifier> ModifierList { get; set; }

    public ParallelModifierList()
    {
      ModifierList = new List<IModifier>();
    }

    public ParallelModifierList(params IModifier[] modifier)
    {
      ModifierList = modifier.ToList();
    }

    public void NextStep(ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      ModifierList.ForEach(m => m.NextStep(sprite, gameTime, camera));
    }

    public bool IsFinished
    {
      get { return ModifierList.All(m => m.IsFinished); }
    }
  }
}
