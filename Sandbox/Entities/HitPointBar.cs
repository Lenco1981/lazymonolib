﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib;
using LazyMonoLib.Primitives;
using Microsoft.Xna.Framework;
using LazyMonoLib.Interfaces;

namespace Sandbox.Entities
{
  public class HitPointBar : ISprite
  {
    private float maxPoints = 1f;
    private float currentPoints;
    private Vector2 size;
    private Vector2 position;

    private FilledRectangle bar;
    private HollowRectangle border;

    private float innerMargin = 0;

    public HitPointBar()
    {
      border = new HollowRectangle();
      border.BorderThickness = 1;     

      bar = new FilledRectangle();
      
    }

    public Color BorderColor { 
      get { return border.Coloration; } 
      set { 
        border.Coloration = value; updateBar(); } }

    public Color BarColor { get { return bar.Coloration; } set { bar.Coloration = value; updateBar(); } }

    public float InnerMargin { get { return innerMargin; } set { innerMargin = value; updateBar(); } }

    public Vector2 Size { get { return size; } set { size = value; updateBar(); } }

    public Vector2 Position { get { return position; } set { position = value; updateBar(); } }

    public float MaxPoints { get { return maxPoints; } set { maxPoints = value; updateBar(); } }

    public float CurrentPoints { get { return currentPoints; } set { currentPoints = value; updateBar(); } }

    public int BorderThickness { get { return border.BorderThickness; } set { border.BorderThickness = value; updateBar(); } }

    private void updateBar()
    {
      border.Position = Position;
      border.Size = Size;
      
      bar.Position = new Vector2(Position.X + innerMargin, Position.Y + innerMargin);
      bar.Size = new Vector2((Size.X - (innerMargin * 2f)) * (currentPoints / maxPoints), Size.Y - (innerMargin * 2f));
            
    }

    public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
    {
      bar.Draw(gameTime, spriteBatch);
      border.Draw(gameTime, spriteBatch);
    }

    public bool DeleteMe { get; set; }

    public void SetSourceRectangle(Rectangle? rectangle)
    {
      
    }


    public void Update(GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState)
    {
      
    }


    public void SetToSubtextureNamed(string subtextureName)
    {

    }
  }
}
