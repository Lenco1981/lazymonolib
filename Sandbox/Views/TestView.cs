﻿using LazyMonoLib;
using LazyMonoLib.Controls;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Sandbox.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Views
{
  public class TestView : BaseView<TestController>
  {
    public TestView(TestController controller)
      : base(controller)
    { 
    
    }

    private Label labelHasDialog;

    public override void InitializeUI()
    {
      labelHasDialog = new Label();
      labelHasDialog.Font = Global.ContentProvider.GetFont("calibri", 14f);
      labelHasDialog.Position = new Vector2(10, 400);

      UI.Add(labelHasDialog);
    }

    public override void InitializeScene()
    {

    }

    public override void InitializeBackground()
    {

    }

    private KeyboardState oldState = new KeyboardState();

    protected override void updateView(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      if (oldState.IsKeyDown(Keys.Space) && keyboardState.IsKeyUp(Keys.Space))
      {
        spacePressed();
      }
      oldState = keyboardState;
    }

    private HelloWorldDialog helloWorldDialog;

    private void spacePressed()
    {
      helloWorldDialog = new HelloWorldDialog();
      helloWorldDialog.Position = new Vector2(100, 100);
      helloWorldDialog.Opening += helloWorldDialog_Opening;
      helloWorldDialog.Closing += helloWorldDialog_Closing;

      CurrentDialog = helloWorldDialog;
    }

    void helloWorldDialog_Closing(ILazyDialogView dialog, LazyMonoLib.Enums.DialogResult result)
    {
      labelHasDialog.Text = "HasDialog<bool>: " + hasCurrentDialog.ToString() + " / currentDialog: " + (CurrentDialog == null ? "<null>" : CurrentDialog.ToString() + " / DialogResult: " + CurrentDialog.DialogResult.ToString());
    }

    void helloWorldDialog_Opening(ILazyDialogView dialog, LazyMonoLib.Enums.DialogResult result)
    {
      labelHasDialog.Text = "HasDialog<bool>: " + hasCurrentDialog.ToString() + " / currentDialog: " + (CurrentDialog == null ? "<null>" : CurrentDialog.ToString() + " / DialogResult: " + CurrentDialog.DialogResult.ToString());

      if (helloWorldDialog != null && helloWorldDialog.DialogResult == LazyMonoLib.Enums.DialogResult.OK)
      {
        labelHasDialog.TextColor = Color.Green;
      }
      else if (helloWorldDialog != null && helloWorldDialog.DialogResult == LazyMonoLib.Enums.DialogResult.Cancel)
        labelHasDialog.TextColor = Color.Crimson;
      else
        labelHasDialog.TextColor = Color.White;
    }




  }
}
