﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using LazyMonoLib.ValueObjects;

namespace LazyMonogameEditorWinForms
{
	public partial class UcSpriteSheetEditor : UserControl
	{
		private string fullFilePath = String.Empty;

		private Image image;

		public UcSpriteSheetEditor()
		{
			InitializeComponent();
		}

		public void Clear()
		{
			fullFilePath = String.Empty;
			txtTexturePath.Text = String.Empty;
			pictureBoxSpriteSheet.Image = new Bitmap(0, 0);
			pictureBoxSpriteSheet.Size = new Size(10, 10);

		}

		public void SetSpriteSheetFile(string filePath)
		{
			if (File.Exists(filePath))
			{
				this.fullFilePath = filePath;


				

				treeView.Nodes.Clear();

				TreeNode root = new TreeNode();
				root.Tag = filePath;
				root.Text = Path.GetFileNameWithoutExtension(filePath);

				treeView.Nodes.Add(root);

				if (File.Exists(getXmlFilePath()))
				{
					SubtextureCollection collection = SubtextureCollection.LoadFromXML(getXmlFilePath());

					image = subtexturesToGraphic(collection, filePath, panelBackgroundColor.BackColor);					

					pictureBoxSpriteSheet.Size = new Size(panelSpriteSheet.Size.Width - 15, panelSpriteSheet.Size.Height - 15); // -new Point(15, 15);

					foreach (SubtextureDefinition def in collection.Values)
					{
						TreeNode node = new TreeNode();
						node.Tag = def;
						node.Text = String.Format("{0} [{1},{2}]", def.Name, def.X, def.Y);

						root.Nodes.Add(node);
					}
				}
				else
					pictureBoxSpriteSheet.Image = Bitmap.FromFile(fullFilePath);
			}
		}

		private Image subtexturesToGraphic(SubtextureCollection collection, string imageFile, Color backgroundColor)
		{
			Image source = Bitmap.FromFile(imageFile);

			Image bitmap = new Bitmap(source.Width + 2, source.Height + 2);


			Graphics g = Graphics.FromImage(bitmap);

			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
			g.FillRectangle(new SolidBrush(backgroundColor), 0, 0, bitmap.Width, bitmap.Height);
			g.DrawImage(source, 1, 1, source.Width, source.Height);

			foreach (SubtextureDefinition subtexture in collection.Values)
			{
				g.DrawRectangle(Pens.Red, subtexture.X+1, subtexture.Y+1, subtexture.Width, subtexture.Height);
			}

			return bitmap;
		}

		private string getXmlFilePath()
		{
			return Path.ChangeExtension(fullFilePath, "xml");
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			SetSpriteSheetFile(txtTexturePath.Text);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (colorDialog1.ShowDialog() == DialogResult.OK)
			{
				panelBackgroundColor.BackColor = colorDialog1.Color;

			}
		}

		private void trackBarZoom_ValueChanged(object sender, EventArgs e)
		{
			float zoomFactor = (float)trackBarZoom.Value / 100f;


			Size newSize = new Size((int)(pictureBoxSpriteSheet.Image.Width * zoomFactor), (int)(pictureBoxSpriteSheet.Image.Height * zoomFactor));

			pictureBoxSpriteSheet.Image = new Bitmap(image, newSize);
		}
	}
}
