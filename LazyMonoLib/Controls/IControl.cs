﻿using LazyMonoLib.EventHandler;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;

namespace LazyMonoLib.Controls
{
  /// <summary>
  /// Defines a general interface for all controls
  /// </summary>
  public interface IControl : IEntity, ILazyDrawable, ILazyUpdateable, IInteraction
  {
    int MarginTop { get; set; }

    int MarginBottom { get; set; }

    int MarginLeft { get; set; }

    int MarginRight { get; set; }

    void SetMargins(int top, int bottom, int left, int right);

    void SetMargins(int topAndBottom, int leftAndRight);

    void SetMargins(int allMargins);

    object Tag { get; set; }
  }
}
