﻿using LazyMonoLib.Controller;
using Sandbox.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Controller
{
  public class HauptmenüController : BaseController<HauptmenüView>
  {
    public HauptmenüController()
    {
      View = new HauptmenüView(this);

      View.InitializeBackground();

      View.InitializeScene();

      View.InitializeUI();
    }

    public override void OnGetActive()
    {
      
    }

    public override void OnGetInactive()
    {
      
    }

    public override bool CanGetInactive()
    {
      return true;
    }
  }
}
