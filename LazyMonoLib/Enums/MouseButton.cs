﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Enums
{
  public enum MouseButton
    {
        /// <summary>
        /// No button ('null-object' pattern)
        /// </summary>
        None,

        /// <summary>
        /// Left mouse button
        /// </summary>
        Left,

        /// <summary>
        /// Middle mouse button
        /// </summary>
        Middle,

        /// <summary>
        /// Right mouse button
        /// </summary>
        Right,

        /// <summary>
        /// Mouse X-Button 1
        /// </summary>
        XButton1,

        /// <summary>
        /// Mouse X-Button 2
        /// </summary>
        XButton2
    }
}
