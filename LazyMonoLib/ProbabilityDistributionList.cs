﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib
{
  public class ProbabilityDistributionList<K>
  {
    private decimal probabilitySum = 0m;

    private List<ProbabilityDistributionEntry<K>> probabilityList = new List<ProbabilityDistributionEntry<K>>();

    public void Add(decimal probability, K entry)
    {
      ProbabilityDistributionEntry<K> newEntry = new ProbabilityDistributionEntry<K>(probability, probabilitySum);

      newEntry.CapsuledEntity = entry;

      probabilityList.Add(newEntry);

      probabilitySum += probability;

      if (probabilitySum > 1m)
        throw new InvalidOperationException("The sum of all probablities can't exceed 1.0");
    }

    public K GetRandomEntry()
    {
      if (probabilitySum != 1m)
        throw new InvalidOperationException("The sum of all probabilities is unequal to 1.0f");
      else
      {
        decimal randomValue = LazyRandom.NextDecimal();

        for (int i = 0; i < probabilityList.Count; i++)
        { 
          ProbabilityDistributionEntry<K> currentEntry = probabilityList[i];

          if (MoreMathHelper.IsInClosedInterval(randomValue, currentEntry.ProbabilityRangeStart, currentEntry.ProbabilityRangeEnd))
            return currentEntry.CapsuledEntity;
        }

        return probabilityList.Last().CapsuledEntity;
      }
    }

    public List<ProbabilityDistributionEntry<K>> Values { get { return probabilityList; } }

    public List<K> Entries { get { return Values.ConvertAll(x => x.CapsuledEntity); } }

    public ProbabilityDistributionEntry<K> FindEntry(K entry)
    {
      return probabilityList.Find(x => x.CapsuledEntity.Equals(entry));
    }

    public int Count { get { return probabilityList.Count; } }
  }

  public class ProbabilityDistributionEntry<K>
  {
    public ProbabilityDistributionEntry(decimal probability, decimal currentProbabilitySum)
    {
      ProbabilityRangeStart = currentProbabilitySum;
      ProbabilityRangeEnd = currentProbabilitySum + probability;
    }

    public K CapsuledEntity { get; set; }

    public decimal ProbabilityRangeStart { get; private set; }

    public decimal ProbabilityRangeEnd { get; private set; }


  }
}
