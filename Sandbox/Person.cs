﻿using LazyMonoLib;
using LazyMonoLib.Enums;
using LazyMonoLib.Modifiers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox
{
  public class Person : Sprite
  {
    public int HitPoints = 100;

    public Color HitBarColor = Color.Green;

    public bool IsMoving { get { return walking != null; } }

    private Rectangle standing;

    private IModifier walkingEastAnimation;
    private IModifier walkingWestAnimation;
    private IModifier walkingNorthAnimation;
    private IModifier walkingSouthAnimation;

    private IModifier fightingEastAnimation;
    private IModifier fightingWestAnimation;
    private IModifier fightingNorthAnimation;
    private IModifier fightingSouthAnimation;


    private LinearMovementModifier walking = null;

    public Person()
    { 
      
    
    }

    public void Initialize(Rectangle standing, IModifier walkingNorthAnimation, IModifier walkingSouthAnimation, IModifier walkingWestAnimation, IModifier walkingEastAnimation)
    {
      this.standing = standing;
      this.walkingEastAnimation = walkingEastAnimation;
      this.walkingNorthAnimation = walkingNorthAnimation;
      this.walkingSouthAnimation = walkingSouthAnimation;
      this.walkingWestAnimation = walkingWestAnimation;
    }

    public void MoveTo(Vector2 destination)
    {
      walking = new LinearMovementModifier(this, this.CenterLocation, destination, 2f);
      AddModifier(walking);
      AddModifier(getWalkingAnimationByRadians(walking.MovementDirectionInRadians));
    }


    private const double PI_180 = Math.PI / 180d;

    private IModifier getWalkingAnimationByRadians(float radians)
    {
      switch (walking.DirectionAsCompassFourth())
      { 
        case CompassFourth.East:
          return walkingEastAnimation;
        case CompassFourth.North:
          return walkingNorthAnimation;
        case CompassFourth.West:
          return walkingWestAnimation;
        case CompassFourth.South:
        default:
          return walkingSouthAnimation;
      }
    }

    public override void Update(GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState)
    {
      base.Update(gameTime, keyboardState, mouseState, gamePadState);

      if (IsMoving && walking.IsFinished)
      {
        SetSourceRectangle(standing);
        ClearModifier();
        walking = null;
      }
    }
  }
}
