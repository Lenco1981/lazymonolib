﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib.Controls
{
  /// <summary>
  /// A simple label class for displaying text.
  /// </summary>
  public class Label : Control
  {
    private Color textColor = Color.White;
    private string text = String.Empty;
    private SpriteFont font;

    /// <summary>
    /// Creates an empty label without any font set.
    /// </summary>
    public Label()
    { }

    /// <summary>
    /// Creates an empty label with the given name without any font set.
    /// </summary>
    public Label(string name)
      : base(name)
    { }

    /// <summary>
    /// Gets or sets the spritefont for this label
    /// </summary>
    public SpriteFont Font
    {
      get { return font; }
      set
      {
        if (font != value)
        {
          font = value;
          fontChanged();
        }
      }
    }

    public string Text
    {
      get { return text; }
      set
      {
        if (text != value)
        {
          text = value;
          textChanged();
        }
      }
    }

    public Color TextColor
    {
      get { return textColor; }
      set
      {
        if (textColor != value)
        {
          textColor = value;
          textColorChanged();
        }
      }
    }


    protected void textChanged()
    {
      if (font != null)
        size = font.MeasureString(text);
    }

    protected virtual void fontChanged()
    {
      if (font != null)
        size = font.MeasureString(text);
    }

    protected virtual void textColorChanged()
    { }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      if (!IsVisible)
        return;

      spriteBatch.DrawString(font, text, camera.Transform(position), TextColor);
    }
  }
}
