﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Interfaces
{
  public interface ILazyDrawable
  {
    void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera);

    bool IsVisible { get; set; }
  }
}
