﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Controls;
using LazyMonoLib.EventHandler;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib
{
  /// <summary>
  /// A sprite containing additional logic for interacting with the user
  /// </summary>
  public class ActableSprite : Sprite, IControl
  {
    /// <summary>
    /// Fires if the mouse pointer enters the sprite
    /// </summary>
    public event MouseEventHandler MouseEnter;

    /// <summary>
    /// Occurs if the mouse pointer leaves the sprite area
    /// </summary>
    public event MouseEventHandler MouseLeave;

    /// <summary>
    /// Occurs if the sprite is clicked with the mouse
    /// </summary>
    public event MouseEventHandler MouseClick;

    private bool isMouseOver = false;

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera)
    {
      if (MouseEnter != null && !isMouseOver && IsHitBy(mouseState, camera))
      {
        isMouseOver = true;
        MouseEnter(this, mouseState);
      }
      else if (MouseLeave != null && isMouseOver && !IsHitBy(mouseState, camera))
      {
        isMouseOver = false;
        MouseLeave(this, mouseState);
      }

      if (mouseState.LeftButton == ButtonState.Pressed && IsHitBy(mouseState, camera) && MouseClick != null)
        MouseClick(this, mouseState);

      base.processModifiers(gameTime, camera);
    }

    #region IControl Members

    public int MarginTop { get; set; }

    public int MarginBottom { get; set; }

    public int MarginLeft { get; set; }

    public int MarginRight { get; set; }

    public void SetMargins(int top, int bottom, int left, int right)
    {

    }

    public void SetMargins(int topAndBottom, int leftAndRight)
    {

    }

    public void SetMargins(int allMargins)
    {

    }

    public object Tag { get; set; }

    #endregion
  }
}
