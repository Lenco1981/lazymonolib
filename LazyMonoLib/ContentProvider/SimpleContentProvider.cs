﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.ValueObjects;

namespace LazyMonoLib.ContentProvider
{

  //TODO: Verschiedene ContentProvider implementieren,
  //z.B. einen rekursiven ContentProvider der alle Verzeichnisse unterhalb von Content\ durchsucht und den
  //Inhalt lädt - inklusive Fortschrittsfeedback!
  public class SimpleContentProvider : BaseContentProvider, IContentProvider
  {
    public SimpleContentProvider(ContentManager contentManager)
      :base(contentManager)
    {

    }

    public Texture2D GetTextureByName(string name)
    {
      if (!textures.ContainsKey(name))
        textures.Add(name, new TextureBundle(content.Load<Texture2D>(name), new List<SubtextureDefinition>()));

      return textures[name].Texture2D;
    }

    public TextureBundle GetTextureBundleByName(string name)
    {
      return null;      
    }

    public void LoadTexture(string filepath)
    {
      string name = getTextureNameFromFilename(filepath);

      if (!textures.ContainsKey(name))
        textures.Add(name, new TextureBundle(content.Load<Texture2D>(filepath), new List<SubtextureDefinition>()));
    }

    public void LoadSpriteFont(string filepath, float size)
    {
      string name = getTextureNameFromFilename(filepath);
      string filename = getTextureNameFromFilename(filepath);

      if (!fonts.ContainsKey(name))
      {
        fonts.Add(name, new Dictionary<float, SpriteFont>());
      }

      fonts[name].Add(size, content.Load<SpriteFont>(filename));
    }


    public void LoadContent()
    {
      throw new NotSupportedException("The SimpleContentProvider does not support automatic content loading. Please use the Load(filepath)-function instead.");
    }
  }
}
