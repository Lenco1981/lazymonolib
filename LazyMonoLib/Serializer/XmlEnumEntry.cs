﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Serializer
{
  public abstract class XmlEnumEntry
  {
    public object Self { get { return this; } }

    public string Name { get; set; }

    public override string ToString()
    {
      return String.Format("<{0}>: {1}", this.GetType().Name, Name);
    }

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;
      else if (obj is XmlEnumEntry)
        return (obj as XmlEnumEntry).Name == this.Name;
      else
        return false;
    }

    public override int GetHashCode()
    {
      return Name.GetHashCode();
    }
  }
}
