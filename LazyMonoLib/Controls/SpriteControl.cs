﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.EventHandler;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Controls
{
  public class SpriteControl : Sprite, IControl
  {
    #region IControl Members

    public int MarginTop { get; set; }

    public int MarginBottom { get; set; }

    public int MarginLeft { get; set; }

    public int MarginRight { get; set; }

    public void SetMargins(int top, int bottom, int left, int right)
    {
      MarginTop = top;
      MarginBottom = bottom;
      MarginLeft = left;
      MarginRight = right;
    }

    public void SetMargins(int topAndBottom, int leftAndRight)
    {
      MarginTop = MarginBottom = topAndBottom;
      MarginLeft = MarginRight = leftAndRight;
    }

    public void SetMargins(int allMargins)
    {
      MarginBottom = MarginLeft = MarginRight = MarginTop = allMargins;
    }

    #endregion

    #region IInteraction Members

    public event MouseEventHandler MouseEnter;

    public event MouseEventHandler MouseLeave;

    public event MouseEventHandler MouseClick;

    #endregion

    public object Tag { get; set; }

    public override void Update(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Input.KeyboardState keyboardState, Microsoft.Xna.Framework.Input.MouseState mouseState, Microsoft.Xna.Framework.Input.GamePadState gamePadState, ICamera camera)
    {
      base.Update(gameTime, keyboardState, mouseState, gamePadState, camera);

      if (IsHitBy(mouseState, camera))
      {
        if (mouseState.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed && MouseClick != null)
          MouseClick(this, mouseState);
      }
    }
  }
}
