﻿using LazyMonoLib.Enums;
using Microsoft.Xna.Framework;
using LazyMonoLib.Utilities;
using LazyMonoLib.EventHandler;

namespace LazyMonoLib.Views
{
  public abstract class AbstractDialog : AbstractView, ILazyDialogView
  {
    private Vector2 size = new Vector2(0, 0);
    private Vector2 position = new Vector2(0, 0);
    

    public AbstractDialog()
      : base()
    { 
    
    }

    public DialogResult DialogResult { get; set; }

    public Vector2 Size { get { return size; } set { size = value; updateCamera(); } }

    public Vector2 CenterPosition
    {
      get
      {
        return new Vector2(
          Position.X + (size.X / 2f),
          Position.Y + (size.Y / 2f));
      }
      set
      {
        Position = new Vector2(
          value.X - (size.X / 2f),
          value.Y - (size.Y / 2f));
      }
    }

    public Vector2 Position
    {
      get { return position; }
      set
      {
        position = value; 
        updateCamera();
      }
    }

    private void updateCamera()
    {
      Camera = new Camera(position, size);
    }

    public event DialogEventHandler Opening;

    public event DialogEventHandler Closing;

    internal void OnDialogOpening()
    {
      if (Opening != null)
        Opening(this, DialogResult);
    }

    internal void OnDialogClosing()
    {
      if (Closing != null)
        Closing(this, DialogResult);
    }
  }
}
