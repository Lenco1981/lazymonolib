﻿using LazyMonoLib.EventArgs;
using LazyMonoLib.Views;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.EventHandler
{
  /// <summary>
  /// Event handler for mouse based events. Containing the object which fired the event and the related mouse state.
  /// </summary>
  public delegate void MouseEventHandler(object sender, MouseState e);
}
