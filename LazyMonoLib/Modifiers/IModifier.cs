﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Abstract interface for any modifier
  /// </summary>
  public interface IModifier
  {
    /// <summary>
    /// Performs the next modifier step
    /// </summary>
    void NextStep(ISprite sprite, GameTime gameTime, ICamera camera);

    /// <summary>
    /// Indicates if the modifier is finished and can be removed
    /// </summary>
    bool IsFinished { get; }
  }
}
