﻿using LazyMonoLib.Enums;
using LazyMonoLib.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Views
{
	/// <summary>
	/// Lowest abstract class for a view. Doesn't need a controller.
	/// Contains the basic layers (background, scene, UI) and corresponding update and draw functions.
	/// </summary>
	public abstract class AbstractView : IView
	{
		public Layers Layers { get; set; }

		/// <summary>
		/// Camera instance for control over view- and viewport-size
		/// </summary>
		public ICamera Camera { get; set; }

		private ILazyDialogView currentDialog = null;

		protected bool hasCurrentDialog = false;

		protected bool isShowingDialog { get { return hasCurrentDialog && currentDialog.DialogResult == DialogResult.None; } }

		/// <summary>
		/// Initializes empty background, scene and UI lists.
		/// </summary>
		public AbstractView()
		{
			Camera = Utilities.Camera.DefaultCamera;

			Layers = new Layers();
		}

		public void Initialize()
		{
			InitializeLayers();
		}

		public abstract void InitializeLayers();

		public ILazyDialogView CurrentDialog
		{
			get { return currentDialog; }
			set
			{
				if (value == null && currentDialog != null)
					currentDialog.As<AbstractDialog>().OnDialogClosing();


				currentDialog = value;
				hasCurrentDialog = value != null && currentDialog.DialogResult == DialogResult.None;

				if (hasCurrentDialog)
					currentDialog.As<AbstractDialog>().OnDialogOpening();
			}
		}


		/// <summary>
		/// Updates the content of this view
		/// </summary>
		public virtual void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
		{
			if (hasCurrentDialog && currentDialog.DialogResult == DialogResult.None)
			{
				currentDialog.Update(gameTime, keyboardState, mouseState, gamePadState);
			}
			else if (hasCurrentDialog && currentDialog.DialogResult != DialogResult.None)
			{
				CurrentDialog = null;
			}
			else
			{
				Layers.Update(gameTime, keyboardState, mouseState, gamePadState, Camera);

				updateView(gameTime, keyboardState, mouseState, gamePadState);
			}
		}

		protected abstract void updateView(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState);


		/// <summary>
		/// Draws the content of this view.
		/// </summary>
		public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
		{
			Layers.Draw(gameTime, spriteBatch, Camera);

			if (hasCurrentDialog && currentDialog.DialogResult == DialogResult.None)
				currentDialog.Draw(gameTime, spriteBatch);
		}

		protected virtual string _(string text)
		{
			return Global.Localize(text);
		}

		protected virtual string _(string text, params object[] parameters)
		{
			return Global.Localize(text, parameters);
		}
	}
}
