﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace LazyMonoLib.Tests
{
  public class CollectionRemovingTest
  {
    [Test]
    public void RemovingTest()
    {

      List<TestEntry> list = new List<TestEntry>();

      list.Add(new TestEntry("1", true));
      list.Add(new TestEntry("2", false));
      list.Add(new TestEntry("3", false));
      list.Add(new TestEntry("4", true));


      for (int i = list.Count - 1; i >= 0; i--)
      {
        if (list[i].Remove)
        {
          Console.WriteLine("removing: " + list[i].Name);
          list.RemoveAt(i);
        }
        else
          Console.WriteLine("entry: " + list[i].Name);
      }

    }



  }

  public class TestEntry
  {
    public string Name = "";

    public bool Remove = false;

    public TestEntry(string name, bool remove)
    {
      Name = name;
      Remove = remove;
    }

    public override string ToString()
    {
      return Name;
    }
  }
}
