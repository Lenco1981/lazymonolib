﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.EventArgs
{
  public class MouseEventArgs_old
  {
    public MouseState State { get; private set; }

    public ButtonState LeftButton { get { return State.LeftButton; } }

    public ButtonState RightButton { get { return State.RightButton; } }

    public ButtonState MiddleButton { get { return State.MiddleButton; } }

    public MouseEventArgs_old(MouseState mouseState)
    {
      State = mouseState;
    }
  }
}
