﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.ValueObjects
{
  public class RectangleTimeSpan
  {
    public Rectangle Rectangle { get; set; }

    public TimeSpan DisplayTimeSpan { get; set; }

    public RectangleTimeSpan(Rectangle rectangle, TimeSpan displayTimeSpan)
    {
      Rectangle = rectangle;
      DisplayTimeSpan = displayTimeSpan;
    }
  }
}
