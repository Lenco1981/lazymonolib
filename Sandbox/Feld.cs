﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib;
using LazyMonoLib.ValueObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sandbox
{
  public class Feld : LazyMonoLib.Interfaces.ILazyDrawable, LazyMonoLib.Interfaces.ILazyUpdateable
  {
    public Feld()
    {
      Mitte = new Sprite();
      Mitte.SetTextureBundle(Global.ContentProvider.GetTextureBundle("walls_concept_1024x1024"));

      Mitte.SetSourceRectangle(Mitte.Subtextures["dummy"].Rectangle);
      Mitte.Size = new Vector2(48, 48);
    }

    

    public Sprite RandLinks { get; set; }

    public Sprite RandRechts { get; set; }

    public Sprite RandOben { get; set; }
    
    public Sprite RandUnten { get; set; }

    public Sprite Mitte { get; set; }

    public int Geräuschpegel { get; set; }

    public int Helligkeit { get; set; }

    public void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      Mitte.Update(gameTime, keyboardState, mouseState, gamePadState);

      //RandLinks.Update(gameTime, keyboardState, mouseState, gamePadState);
      //RandRechts.Update(gameTime, keyboardState, mouseState, gamePadState);
      //RandUnten.Update(gameTime, keyboardState, mouseState, gamePadState);
      //RandOben.Update(gameTime, keyboardState, mouseState, gamePadState);
    }


    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      Mitte.Draw(gameTime, spriteBatch);

      //RandLinks.Draw(gameTime, spriteBatch);
      //RandRechts.Draw(gameTime, spriteBatch);
      //RandOben.Draw(gameTime, spriteBatch);
      //RandUnten.Draw(gameTime, spriteBatch);
    }
  }
}
