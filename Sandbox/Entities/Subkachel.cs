﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Entities
{
  public class Subkachel
  {
    public bool Walkable { get; set; }

    public int WalkingCosts { get; set; }

    public bool HasPerson { get; set; }

    public Texture2D Texture { get; set; }

    public Person Person { get; set; }
  }
}
