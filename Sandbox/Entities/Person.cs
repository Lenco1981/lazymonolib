﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Modifiers;
using LazyMonoLib.Enums;

namespace Sandbox.Entities
{
  /// <summary>
  /// Abstract base class for zombies and survivors
  /// </summary>
  public abstract class Person : LazyMonoLib.Entity
  {
    public int HitPoints { get; set; }

    public Color HitBarColor { get; set; }

    public Texture2D Texture { get; set; }

    public Rectangle? SourceRectangle { get; set; }

    private Rectangle standing;

    private IModifier walkingEastAnimation;
    private IModifier walkingWestAnimation;
    private IModifier walkingNorthAnimation;
    private IModifier walkingSouthAnimation;

    private IModifier fightingEastAnimation;
    private IModifier fightingWestAnimation;
    private IModifier fightingNorthAnimation;
    private IModifier fightingSouthAnimation;

    private LinearMovementModifier walking = null;

    public void Initialize(Rectangle standing, IModifier walkingNorthAnimation, IModifier walkingSouthAnimation, IModifier walkingWestAnimation, IModifier walkingEastAnimation)
    {
      this.standing = standing;
      this.walkingEastAnimation = walkingEastAnimation;
      this.walkingNorthAnimation = walkingNorthAnimation;
      this.walkingSouthAnimation = walkingSouthAnimation;
      this.walkingWestAnimation = walkingWestAnimation;
    }

    public override void Draw(GameTime gameTime,SpriteBatch spriteBatch)
    {
      
    }

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {
      
    }

    public void MoveTo(Vector2 destination)
    {
      walking = new LinearMovementModifier(this, this.CenterLocation, destination, 2f);
      AddModifier(walking);
      AddModifier(getWalkingAnimationByRadians(walking.MovementDirectionInRadians));
    }

    private IModifier getWalkingAnimationByRadians(float radians)
    {
      switch (walking.DirectionAsCompassFourth())
      {
        case CompassFourth.East:
          return walkingEastAnimation;
        case CompassFourth.North:
          return walkingNorthAnimation;
        case CompassFourth.West:
          return walkingWestAnimation;
        case CompassFourth.South:
        default:
          return walkingSouthAnimation;
      }
    }
  }
}
