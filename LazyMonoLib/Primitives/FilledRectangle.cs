﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib.Primitives
{
  public class FilledRectangle : Entity
  {
    private Texture2D texture = null;
    private Color fillColor = Color.White;

    public Color FillColor
    {
      get { return fillColor; }
      set
      {
        if (fillColor != value)
        {
          fillColor = value;
          FillColorValueChanged();
        }
      }
    }

    public virtual void FillColorValueChanged()
    {
      texture = new Texture2D(Global.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);

      texture.SetData(new[] { FillColor });
    }

    public FilledRectangle()
      : this(Color.White)
    { }

    public FilledRectangle(Color fillColor)
    {
      FillColor = fillColor;
      FillColorValueChanged();
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      spriteBatch.Draw(texture, getDestinationRectangle(camera), Coloration);
    }

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera)
    {
      
    }
  }
}
