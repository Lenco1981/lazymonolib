﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LazyMonoLib.EventHandler;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Controls
{
  public abstract class Control : Entity, IControl
  {
    public Control()
    {

    }

    public Control(string name)
    {
      this.Name = name;
    }

    public object Tag { get; set; }

    public virtual int MarginTop { get; set; }

    public virtual int MarginBottom { get; set; }

    public virtual int MarginLeft { get; set; }

    public virtual int MarginRight { get; set; }

    public virtual void SetMargins(int top, int bottom, int left, int right)
    {
      MarginTop = top;
      MarginBottom = bottom;
      MarginLeft = left;
      MarginRight = right;
    }

    public virtual void SetMargins(int topAndBottom, int leftAndRight)
    {
      SetMargins(topAndBottom, topAndBottom, leftAndRight, leftAndRight);
    }

    public virtual void SetMargins(int allMargins)
    {
      SetMargins(allMargins, allMargins, allMargins, allMargins);
    }

    public event MouseEventHandler MouseEnter;

    public event MouseEventHandler MouseLeave;

    public event MouseEventHandler MouseClick;

    private MouseState oldMouseState = new MouseState();


    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera)
    {
      if (MouseEnter != null && IsHitBy(mouseState,camera) && !IsHitBy(oldMouseState, camera))
        MouseEnter(this, mouseState);

      if (MouseLeave != null && !IsHitBy(mouseState,camera) && IsHitBy(oldMouseState, camera))
        MouseLeave(this, mouseState);

      if (MouseClick != null && IsHitBy(mouseState,camera) && oldMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
        MouseClick(this, mouseState);

      oldMouseState = mouseState;
    }
  }
}
