﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LazyMonoLib.Utilities
{
  public interface ICamera
  {
    Vector2 Transform(Vector2 point);

    Rectangle Transform(Rectangle rectangle);

    Vector2 CenterPosition { get; }
  }
}
