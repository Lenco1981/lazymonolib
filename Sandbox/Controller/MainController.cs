﻿using LazyMonoLib;
using LazyMonoLib.ContentProvider;
using LazyMonoLib.Controller;
using Sandbox.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Controller
{
  public class MainController : BaseController<MainScreenView>
  {
    public MainController()
    {
      View = new MainScreenView(this);

      View.InitializeBackground();

      View.InitializeScene();

      View.InitializeUI();

      
    }

    public override void OnGetActive()
    {
      ((MainScreenView)View).StartRound();
    }

    //public void SetCurrentPlayingField(Spielfeld)

    public override void OnGetInactive()
    {
      
    }

    public override bool CanGetInactive()
    {
      return true;
    }


  }
}
