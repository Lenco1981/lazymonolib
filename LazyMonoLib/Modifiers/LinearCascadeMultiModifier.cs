﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Utilities;

namespace LazyMonoLib.Modifiers
{
  /// <summary>
  /// Executes multiple modifiers in a list one after one
  /// </summary>
  public class LinearCascadeMultiModifier : IModifier
  {
    public List<IModifier> ModifierList { get; set; }

    private int currentIndex = 0;

    public LinearCascadeMultiModifier()
    {
      ModifierList = new List<IModifier>();
    }

    public void NextStep(LazyMonoLib.Interfaces.ISprite sprite, Microsoft.Xna.Framework.GameTime gameTime, ICamera camera)
    {
      if (currentIndex < ModifierList.Count)
      {
        ModifierList[currentIndex].NextStep(sprite, gameTime, camera);

        if (ModifierList[currentIndex].IsFinished)
          currentIndex++;
      }
    }

    public bool IsFinished
    {
      get { return (currentIndex == ModifierList.Count - 1) && ModifierList[currentIndex].IsFinished; }
    }
  }
}
