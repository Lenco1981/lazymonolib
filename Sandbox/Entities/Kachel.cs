﻿using LazyMonoLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Entities
{
  /// <summary>
  /// Kachel ist die Datenstruktur für ein 40x40 Pixel großen Abschnitt des Spielfeldes.
  /// Dieses Feld besteht aus einer Hintergrundtexture und bis zu 25 8x8 Pixel große "Structures" 
  /// die auf dem Feld gebaut werden (Gräben, Zäune, Wände, usw.)
  /// Es können bis zu zwei Menschen bzw. bis zu 9 Zombies auf diesem Feld stehen.
  /// </summary>
  public class Kachel : Entity, LazyMonoLib.Interfaces.ILazyDrawable, LazyMonoLib.Interfaces.ILazyUpdateable
  {
    public static Dictionary<SurvivorPosition, Vector2> SurvivorPositionOffsets = new Dictionary<SurvivorPosition, Vector2> 
    {
      {SurvivorPosition.TopLeft, new Vector2(12,12)},
      {SurvivorPosition.TopCenter, new Vector2(20, 12)},
      {SurvivorPosition.TopLeft, new Vector2(28,12)},

      {SurvivorPosition.MiddleLeft, new Vector2(12,20)},
      {SurvivorPosition.MiddleCenter, new Vector2(20,20)},
      {SurvivorPosition.MiddleRight, new Vector2(28,20)},

      {SurvivorPosition.BottomLeft, new Vector2(12,28)},
      {SurvivorPosition.BottomCenter, new Vector2(20, 28)},
      {SurvivorPosition.BottomRight, new Vector2(28,28)}
    };

    public Texture2D FloorTileTexture;

    public List<Survivor> Survivors = new List<Survivor>();

    public void SurvivorArrives(Survivor survivor)
    {
      if (Survivors.Count == 0)
      {
        Survivors.Add(survivor);

        if (survivor.CenterLocation != calcPositionWithOffset(SurvivorPosition.MiddleCenter))
        {
          survivor.MoveTo(calcPositionWithOffset(SurvivorPosition.MiddleCenter));
        }
      }
      else if (Survivors.Count == 1)
      {
        Survivors.Add(survivor);

        survivor.MoveTo(calcPositionWithOffset(SurvivorPosition.BottomLeft));
        Survivors[0].MoveTo(calcPositionWithOffset(SurvivorPosition.TopRight));
      }
      else
        throw new InvalidOperationException("Can't move more than one person to this kachel... :-P");
    }

    protected Vector2 calcPositionWithOffset(SurvivorPosition position)
    {
      return Position + SurvivorPositionOffsets[position];
    }

    public Vector2 getNextSurviorPosition()
    {
      if (Survivors.Count == 0)
        return SurvivorPositionOffsets[SurvivorPosition.MiddleCenter];
      else
        return new Vector2(0, 0);
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      spriteBatch.Draw(FloorTileTexture, destinationRectangle, sourceRectangle, coloration);
    }

    public void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState)
    {

    }

    public enum SurvivorPosition { TopCenter, TopLeft, TopRight, MiddleRight, MiddleLeft, MiddleCenter, BottomCenter, BottomLeft, BottomRight };
  }
}
