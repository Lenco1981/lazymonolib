﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace LazyMonoLib.ValueObjects
{
	/// <summary>
	/// Contains a list of subtextures
	/// </summary>
	public class SubtextureCollection
	{
		/// <summary>
		/// Creates a new subtexture collection
		/// </summary>
		public SubtextureCollection()
		{
			Values = new List<SubtextureDefinition>();
		}

		[XmlElement("Subtexture")]
		public List<SubtextureDefinition> Values { get; set; }

		/// <summary>
		/// Loads the subtextures from a XML file
		/// </summary>
		public static SubtextureCollection LoadFromXML(string filename)
		{
			if (File.Exists(filename))
			{
				TextReader reader = File.OpenText(filename);

				XmlSerializer serializer = new XmlSerializer(typeof(SubtextureCollection));

				var result = serializer.Deserialize(reader);

				reader.Close();

				return (SubtextureCollection)result;
			}
			else
				return new SubtextureCollection();
		}

		/// <summary>
		/// Writes the collection to a XML file
		/// </summary>
		public static void WriteToXML(string filename, SubtextureCollection collection)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(SubtextureCollection));

			StringWriter writer = new StringWriter();

			serializer.Serialize(writer, collection);

			File.WriteAllText(filename, writer.ToString());
		}
	}
}
