﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Controls;
using LazyMonoLib.Interfaces;
using LazyMonoLib.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LazyMonoLib.Primitives
{
  public class Line : Entity, IEntity, IModifiable
  {
    BasicEffect basicEffect;
    VertexPositionColor[] vertices;

    public Line()
    {
      vertices = new VertexPositionColor[2];


      basicEffect = new BasicEffect(Global.GraphicsDevice);
      basicEffect.VertexColorEnabled = true;
      
      basicEffect.Projection = Matrix.CreateOrthographicOffCenter
         (0, Global.GraphicsDevice.Viewport.Width,     // left, right
          Global.GraphicsDevice.Viewport.Height, 0,    // bottom, top
          0, 1);                                       // near, far plane
    }

    public VertexPositionColor Start { get { return vertices[0]; } set { vertices[0] = value; } }

    public VertexPositionColor End { get { return vertices[1]; } set { vertices[1] = value; } }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, ICamera camera)
    {
      basicEffect.CurrentTechnique.Passes[0].Apply();
      Global.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);
    }

    public override void Update(GameTime gameTime, KeyboardState keyboardState, MouseState mouseState, GamePadState gamePadState, ICamera camera)
    {
      
    }

    #region IModifiable Members

    public void AddModifier(Modifiers.IModifier modifier)
    {
      throw new NotImplementedException();
    }

    public void RemoveModifier(Modifiers.IModifier modifier)
    {
      throw new NotImplementedException();
    }

    public void ClearModifier()
    {
      throw new NotImplementedException();
    }

    public bool HasModifier(Modifiers.IModifier modifier)
    {
      throw new NotImplementedException();
    }

    public bool HasModifierOfType<K>()
    {
      throw new NotImplementedException();
    }

    public K GetFirstModifierOfType<K>()
    {
      throw new NotImplementedException();
    }

    public List<K> GetModifierOfType<K>()
    {
      throw new NotImplementedException();
    }

    public int ModifierCount
    {
      get { throw new NotImplementedException(); }
    }

    #endregion
  }
}
