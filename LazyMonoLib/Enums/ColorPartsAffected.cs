﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LazyMonoLib.Enums
{
  [Flags]
  public enum ColorPartsAffected
  {
    None = 0,
    Red = 1,
    Green = 2,
    Blue = 4,
    Alpha = 8,
    All = Red + Green + Blue + Alpha
  }
}
