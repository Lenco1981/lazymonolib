﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LazyMonoLib.Enums;
using LazyMonoLib.Views;

namespace LazyMonoLib.EventHandler
{
  public delegate void DialogEventHandler(ILazyDialogView dialog, DialogResult result);
}
