﻿using System;
using System.Collections.Generic;
using System.Enums;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyMonogameLib.Project
{
	public class ProjectEntryFactory
	{
		public static IProjectEntry CreateFromPath(string path)
		{
			if (File.Exists(path))
			{
				return handleAsFile(path);
			}
			else if (Directory.Exists(path))
			{
				return handleAsDirectory(path);
			}
			else
				throw new InvalidOperationException("Path or File doesn't exist");
		}

		private static IProjectEntry handleAsDirectory(string path)
		{
			DummyProjectEntry entry = new DummyProjectEntry();

			entry.DisplayName = path.PartAfterLast(@"\", ReturnValueOnNotFound.ReturnEntireString);
			entry.FullPath = path;

			foreach (string directory in Directory.GetDirectories(path))
			{
				entry.Childs.Add(CreateFromPath(directory));
			}

			foreach (string file in Directory.GetFiles(path))
			{
				entry.Childs.Add(CreateFromPath(file));
			}

			return entry;
		}

		private static IProjectEntry handleAsFile(string path)
		{
			DummyProjectEntry entry = new DummyProjectEntry();

			entry.DisplayName = Path.GetFileName(path);
			entry.FullPath = path;

			return entry;
		}
	}
}
